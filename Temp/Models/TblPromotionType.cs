﻿using System;
using System.Collections.Generic;

namespace Temp.Models
{
    public partial class TblPromotionType
    {
        public int PromotionTypeId { get; set; }
        public string PromotionTypeName { get; set; }
    }
}
