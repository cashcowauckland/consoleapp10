﻿using System;
using System.Collections.Generic;

namespace Temp.Models
{
    public partial class TblEmailServer
    {
        public int EmailServerId { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Smtp { get; set; }
        public string Port { get; set; }
        public bool? Ssl { get; set; }
    }
}
