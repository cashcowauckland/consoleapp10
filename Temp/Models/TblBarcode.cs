﻿using System;
using System.Collections.Generic;

namespace Temp.Models
{
    public partial class TblBarcode
    {
        public int BarcodeId { get; set; }
        public int? ItemId { get; set; }
        public string Barcode { get; set; }
    }
}
