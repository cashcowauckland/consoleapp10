﻿using System;
using System.Collections.Generic;

namespace Temp.Models
{
    public partial class TblConnectionReport
    {
        public int ConnectionReportId { get; set; }
        public string Time { get; set; }
        public string Date { get; set; }
        public string Status { get; set; }
    }
}
