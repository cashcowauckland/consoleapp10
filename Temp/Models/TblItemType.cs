﻿using System;
using System.Collections.Generic;

namespace Temp.Models
{
    public partial class TblItemType
    {
        public int ItemTypeId { get; set; }
        public string TypeName { get; set; }
        public string GroupName { get; set; }

        public TblItemTypePosition TblItemTypePosition { get; set; }
    }
}
