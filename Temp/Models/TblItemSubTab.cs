﻿using System;
using System.Collections.Generic;

namespace Temp.Models
{
    public partial class TblItemSubTab
    {
        public int ItemSubTabId { get; set; }
        public int? ItemId { get; set; }
        public int? SubTabId { get; set; }
        public int? PositionNum { get; set; }
        public int? Color { get; set; }
        public int? FontColor { get; set; }
        public double? FontSize { get; set; }

        public TblItem Item { get; set; }
    }
}
