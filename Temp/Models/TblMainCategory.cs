﻿using System;
using System.Collections.Generic;

namespace Temp.Models
{
    public partial class TblMainCategory
    {
        public int MainCategoryId { get; set; }
        public string MainCategoryName { get; set; }
    }
}
