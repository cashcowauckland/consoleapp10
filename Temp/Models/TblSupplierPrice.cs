﻿using System;
using System.Collections.Generic;

namespace Temp.Models
{
    public partial class TblSupplierPrice
    {
        public int SupplierPriceId { get; set; }
        public int? SupplierId { get; set; }
        public int? ItemId { get; set; }
        public decimal? Price { get; set; }
        public string SupplierItemCode { get; set; }
        public string SupplierItemName { get; set; }
        public string SupplierItemBarcode { get; set; }
    }
}
