﻿using System;
using System.Collections.Generic;

namespace Temp.Models
{
    public partial class TblDailyRecord
    {
        public int DailyRecordId { get; set; }
        public string Date { get; set; }
        public int? VoidEntireNum { get; set; }
        public int? NoSaleNum { get; set; }
    }
}
