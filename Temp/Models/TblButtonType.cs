﻿using System;
using System.Collections.Generic;

namespace Temp.Models
{
    public partial class TblButtonType
    {
        public int ButtonTypeId { get; set; }
        public int? NumOfButton { get; set; }
    }
}
