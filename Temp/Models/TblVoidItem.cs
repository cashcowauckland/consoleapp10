﻿using System;
using System.Collections.Generic;

namespace Temp.Models
{
    public partial class TblVoidItem
    {
        public int VoildId { get; set; }
        public int? ItemId { get; set; }
        public int? OrderId { get; set; }
        public decimal? Qty { get; set; }
        public string VoidDate { get; set; }
        public string VoidTime { get; set; }
        public int? StaffId { get; set; }
        public bool? Pending { get; set; }
        public bool? VoidAll { get; set; }
    }
}
