﻿using System;
using System.Collections.Generic;

namespace Temp.Models
{
    public partial class TblTableTran
    {
        public int TranId { get; set; }
        public int? OrderId { get; set; }
    }
}
