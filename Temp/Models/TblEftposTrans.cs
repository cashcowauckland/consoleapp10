﻿using System;
using System.Collections.Generic;

namespace Temp.Models
{
    public partial class TblEftposTrans
    {
        public int EftposTransId { get; set; }
        public decimal? Purchase { get; set; }
        public decimal? Cashout { get; set; }
        public decimal? Refund { get; set; }
        public int? MerchantId { get; set; }
        public string TransType { get; set; }
        public int? OrderId { get; set; }
        public DateTime? CreatedOn { get; set; }
    }
}
