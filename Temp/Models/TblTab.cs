﻿using System;
using System.Collections.Generic;

namespace Temp.Models
{
    public partial class TblTab
    {
        public int TabId { get; set; }
        public string TabName { get; set; }
        public int? PositionNum { get; set; }
    }
}
