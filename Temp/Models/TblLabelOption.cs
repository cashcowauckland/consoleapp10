﻿using System;
using System.Collections.Generic;

namespace Temp.Models
{
    public partial class TblLabelOption
    {
        public int LabelOptionId { get; set; }
        public string LabelOptionName { get; set; }
        public int? DescPrintSetId { get; set; }
        public int? Desc2PrintSetId { get; set; }
        public int? PricePrintSetId { get; set; }
        public int? CodePrintSetId { get; set; }
        public bool? PrintBarcode { get; set; }
    }
}
