﻿using System;
using System.Collections.Generic;

namespace Temp.Models
{
    public partial class TblPromotion
    {
        public int PromotionId { get; set; }
        public string Description { get; set; }
        public int? PromotionTypeId { get; set; }
        public int? QtyBuy { get; set; }
        public int? QtyGet { get; set; }
        public decimal? DiscountP { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public int? Limitation { get; set; }
        public bool? MultipleGroup { get; set; }
    }
}
