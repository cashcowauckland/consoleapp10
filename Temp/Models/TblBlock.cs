﻿using System;
using System.Collections.Generic;

namespace Temp.Models
{
    public partial class TblBlock
    {
        public int BlockId { get; set; }
        public string BlockName { get; set; }
        public int? XCoordinate { get; set; }
        public int? YCoordinate { get; set; }
        public int? Height { get; set; }
        public int? Width { get; set; }
    }
}
