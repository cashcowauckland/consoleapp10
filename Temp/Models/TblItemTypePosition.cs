﻿using System;
using System.Collections.Generic;

namespace Temp.Models
{
    public partial class TblItemTypePosition
    {
        public int ItemPositionId { get; set; }
        public int? ItemId { get; set; }
        public int? ItemTypeId { get; set; }
        public int? Position { get; set; }
        public bool? IsScale { get; set; }

        public TblItemType ItemPosition { get; set; }
    }
}
