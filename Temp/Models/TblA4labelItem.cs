﻿using System;
using System.Collections.Generic;

namespace Temp.Models
{
    public partial class TblA4labelItem
    {
        public int LabelItemId { get; set; }
        public string ItemName { get; set; }
        public int? PositionX { get; set; }
        public int? PositionY { get; set; }
        public int? Font { get; set; }
        public bool? Selected { get; set; }
        public int? BarcodeHeight { get; set; }
    }
}
