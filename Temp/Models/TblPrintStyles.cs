﻿using System;
using System.Collections.Generic;

namespace Temp.Models
{
    public partial class TblPrintStyles
    {
        public int PrintSettingId { get; set; }
        public string PrintSettingName { get; set; }
        public string FontType { get; set; }
        public string FontSize { get; set; }
        public string Alignment { get; set; }
        public bool? Emphasize { get; set; }
    }
}
