﻿using System;
using System.Collections.Generic;

namespace Temp.Models
{
    public partial class TblActivityLog
    {
        public int ActivityId { get; set; }
        public string ActivityDate { get; set; }
        public string ActivityTime { get; set; }
        public string ActivityLog { get; set; }
        public int? ActivityType { get; set; }
        public int? StaffId { get; set; }
        public int? CustomerId { get; set; }
        public int? TerminalId { get; set; }
    }
}
