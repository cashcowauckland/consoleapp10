﻿using System;
using System.Collections.Generic;

namespace Temp.Models
{
    public partial class TblKitchenSetup
    {
        public byte SetupId { get; set; }
        public byte NumOfDisplay { get; set; }
        public byte LangOpt { get; set; }
        public short TimeOutMinutes { get; set; }
        public bool IsCategory { get; set; }
        public string LoginUserName { get; set; }
        public bool? IsPrintReceipt { get; set; }
    }
}
