﻿using System;
using System.Collections.Generic;

namespace Temp.Models
{
    public partial class TblStockRequest
    {
        public int RequestId { get; set; }
        public string ShopCode { get; set; }
        public int? ItemId { get; set; }
        public decimal? Qty { get; set; }
        public string Comment { get; set; }
        public string Result { get; set; }
        public string Date { get; set; }
        public string HqComment { get; set; }
        public bool? Incoming { get; set; }
        public decimal? DispachQty { get; set; }
        public decimal? AckQty { get; set; }
        public bool? Ready { get; set; }
        public int? OrigRequestId { get; set; }
    }
}
