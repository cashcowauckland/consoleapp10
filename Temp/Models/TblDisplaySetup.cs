﻿using System;
using System.Collections.Generic;

namespace Temp.Models
{
    public partial class TblDisplaySetup
    {
        public int ControlId { get; set; }
        public string ControlType { get; set; }
        public string Font { get; set; }
        public string Style { get; set; }
        public int? Size { get; set; }
        public int? Color { get; set; }
        public string Text { get; set; }
        public string ImgPath { get; set; }
        public int? Height { get; set; }
        public int? Width { get; set; }
        public int? CoordinateX { get; set; }
        public int? CoordinateY { get; set; }
        public int? Duration { get; set; }
    }
}
