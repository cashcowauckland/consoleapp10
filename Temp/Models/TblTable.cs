﻿using System;
using System.Collections.Generic;

namespace Temp.Models
{
    public partial class TblTable
    {
        public int TableId { get; set; }
        public int? OrderId { get; set; }
        public int? TableNum { get; set; }
        public int? StaffId { get; set; }
        public int? CoordinateX { get; set; }
        public int? CoordinateY { get; set; }
        public int? Size { get; set; }
        public string Shape { get; set; }
        public int? ReserveId { get; set; }
        public int? TableStatusId { get; set; }
        public int? Guest { get; set; }
        public bool? Lock { get; set; }
    }
}
