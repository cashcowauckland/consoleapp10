﻿using System;
using System.Collections.Generic;

namespace Temp.Models
{
    public partial class TblCostMargin
    {
        public int MarginId { get; set; }
        public decimal? CostTo { get; set; }
        public decimal? Price { get; set; }
    }
}
