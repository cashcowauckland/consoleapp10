﻿using System;
using System.Collections.Generic;

namespace Temp.Models
{
    public partial class TblItemVolumePrice
    {
        public int ItemVolumePriceId { get; set; }
        public int? ItemId { get; set; }
        public int? VolumeTo { get; set; }
        public decimal? Price { get; set; }
    }
}
