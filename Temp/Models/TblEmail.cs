﻿using System;
using System.Collections.Generic;

namespace Temp.Models
{
    public partial class TblEmail
    {
        public int EmailId { get; set; }
        public string MailFrom { get; set; }
        public string MailTo { get; set; }
        public string MailCc { get; set; }
        public string MailBcc { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string Attachment { get; set; }
        public string MailServer { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string WeekDay { get; set; }
        public string Time { get; set; }
    }
}
