﻿using System;
using System.Collections.Generic;

namespace Temp.Models
{
    public partial class TblTranItem
    {
        public int TranItemId { get; set; }
        public int? TranId { get; set; }
        public string ItemName { get; set; }
        public string ItemQty { get; set; }
        public decimal? Amount { get; set; }
        public int? OrderId { get; set; }
        public int? ItemId { get; set; }
        public int? TerminalId { get; set; }
        public int? PaymentId { get; set; }
    }
}
