﻿using System;
using System.Collections.Generic;

namespace Temp.Models
{
    public partial class TblIpeftpos
    {
        public int IpeftposId { get; set; }
        public string IpeftposName { get; set; }
        public string IpeftposIp { get; set; }
        public string IpeftposPort { get; set; }
        public bool? IsPrimary { get; set; }
    }
}
