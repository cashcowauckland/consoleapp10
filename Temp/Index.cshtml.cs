﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Temp.Models;

namespace Temp
{
    public class IndexModel : PageModel
    {
        private readonly Temp.Models.PosSystemContext _context;

        public IndexModel(Temp.Models.PosSystemContext context)
        {
            _context = context;
        }

        public IList<TblItemType> TblItemType { get;set; }

        public async Task OnGetAsync()
        {
            TblItemType = await _context.TblItemType.ToListAsync();
        }
    }
}
