﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Temp.Models;

namespace Temp
{
    public class CreateModel : PageModel
    {
        private readonly Temp.Models.PosSystemContext _context;

        public CreateModel(Temp.Models.PosSystemContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public TblItemType TblItemType { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.TblItemType.Add(TblItemType);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}