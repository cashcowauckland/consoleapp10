﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Temp.Models;

namespace Temp
{
    public class DeleteModel : PageModel
    {
        private readonly Temp.Models.PosSystemContext _context;

        public DeleteModel(Temp.Models.PosSystemContext context)
        {
            _context = context;
        }

        [BindProperty]
        public TblItemType TblItemType { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            TblItemType = await _context.TblItemType.FirstOrDefaultAsync(m => m.ItemTypeId == id);

            if (TblItemType == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            TblItemType = await _context.TblItemType.FindAsync(id);

            if (TblItemType != null)
            {
                _context.TblItemType.Remove(TblItemType);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
