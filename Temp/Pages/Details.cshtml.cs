﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Temp.Models;

namespace Temp
{
    public class DetailsModel : PageModel
    {
        private readonly Temp.Models.PosSystemContext _context;

        public DetailsModel(Temp.Models.PosSystemContext context)
        {
            _context = context;
        }

        public TblItemType TblItemType { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            TblItemType = await _context.TblItemType.FirstOrDefaultAsync(m => m.ItemTypeId == id);

            if (TblItemType == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
