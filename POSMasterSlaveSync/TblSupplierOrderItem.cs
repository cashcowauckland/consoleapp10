﻿namespace Temp.Models
{
    public partial class TblSupplierOrderItem
    {
        public int SupplierOrderItemId { get; set; }
        public int? SupplierOrderId { get; set; }
        public decimal? Price { get; set; }
        public int? ItemId { get; set; }
        public int? Qty { get; set; }
        public int? AckQty { get; set; }
        public string Comment { get; set; }
        public string CommentOrder { get; set; }
    }
}
