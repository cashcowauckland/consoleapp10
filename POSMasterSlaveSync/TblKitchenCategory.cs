﻿namespace Temp.Models
{
    public partial class TblKitchenCategory
    {
        public byte SetupId { get; set; }
        public int CategoryId { get; set; }
    }
}
