﻿namespace Temp.Models
{
    public partial class TblGroupDiscount
    {
        public int GroupDiscountId { get; set; }
        public decimal? DiscountP { get; set; }
        public decimal? DiscountD { get; set; }
        public int? TerminalId { get; set; }
    }
}
