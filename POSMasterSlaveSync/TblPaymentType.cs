﻿namespace Temp.Models
{
    public partial class TblPaymentType
    {
        public int PaymentTypeId { get; set; }
        public string PaymentTypeName { get; set; }
        public bool? UseEftpos { get; set; }
        public bool? OpenCashdraw { get; set; }
        public bool? HidDisplay { get; set; }
    }
}
