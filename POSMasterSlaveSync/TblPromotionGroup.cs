﻿namespace Temp.Models
{
    public partial class TblPromotionGroup
    {
        public int PromotionGroupId { get; set; }
        public int? PromotionId { get; set; }
        public int? QtyBuyGet { get; set; }
        public int? GroupNo { get; set; }
    }
}
