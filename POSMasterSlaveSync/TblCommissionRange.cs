﻿namespace Temp.Models
{
    public partial class TblCommissionRange
    {
        public int RangeId { get; set; }
        public decimal? SalesTo { get; set; }
        public decimal? CommissionRate { get; set; }
    }
}
