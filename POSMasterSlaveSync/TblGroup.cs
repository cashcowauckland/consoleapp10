﻿namespace Temp.Models
{
    public partial class TblGroup
    {
        public int GroupId { get; set; }
        public string GroupName { get; set; }
    }
}
