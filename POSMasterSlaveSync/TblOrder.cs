﻿namespace Temp.Models
{
    public partial class TblOrder
    {
        public int OrderId { get; set; }
        public string OrderDate { get; set; }
        public string OrderTime { get; set; }
        public decimal? TotalSales { get; set; }
        public decimal? CashoutAmount { get; set; }
        public int? StaffId { get; set; }
        public decimal? DiscountP { get; set; }
        public decimal? DiscountD { get; set; }
        public decimal? SurchargeP { get; set; }
        public int? TerminalId { get; set; }
        public int? TagNum { get; set; }
        public int? CustomerId { get; set; }
        public string Comment { get; set; }
        public string Barcode { get; set; }
        public int? SaleTypeId { get; set; }
        public string DueDate { get; set; }
        public int? SaleStaffId { get; set; }
        public decimal? DiscountPaidAmount { get; set; }
        public string EthnicId { get; set; }
        public string TaxContent { get; set; }
        public string Tender { get; set; }
        public string Change { get; set; }
    }
}
