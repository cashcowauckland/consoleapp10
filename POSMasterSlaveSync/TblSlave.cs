﻿namespace Temp.Models
{
    public partial class TblSlave
    {
        public int SlaveId { get; set; }
        public string SlaveIp { get; set; }
        public string DatabaseName { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public int? QueryId { get; set; }
        public string MainDatabase { get; set; }
        public int? PosUpdateId { get; set; }
    }
}
