﻿namespace Temp.Models
{
    public partial class TblShop
    {
        public int ShopId { get; set; }
        public string ShopName { get; set; }
        public string ShopAddress { get; set; }
        public int? PointPerDollar { get; set; }
        public decimal? PointValue { get; set; }
        public string PhoneNo { get; set; }
        public string GstNo { get; set; }
        public int? ButtonTypeId { get; set; }
        public string UpdateDate { get; set; }
        public string ButtonPressedDate { get; set; }
        public string ButtonPressedTime { get; set; }
        public bool? SlaveUpdate { get; set; }
        public string Version { get; set; }
        public decimal? GstRate { get; set; }
        public bool? Act { get; set; }
        public bool? NewRequest { get; set; }
        public string LastStockTake { get; set; }
        public string AcctName { get; set; }
        public string AcctNo { get; set; }
        public int? InvoiceNo { get; set; }
        public bool? InvoiceDetails { get; set; }
        public string CurrencyName { get; set; }
        public string CurrencySymbol { get; set; }
        public string GstName { get; set; }
        public string TemplateText { get; set; }
        public bool? ShowPaySummary { get; set; }
        public bool? ShowItemSummary { get; set; }
        public string Data { get; set; }
        public string Ref { get; set; }
        public string PlimusId { get; set; }
    }
}
