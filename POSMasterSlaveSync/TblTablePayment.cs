﻿namespace Temp.Models
{
    public partial class TblTablePayment
    {
        public int PaymentId { get; set; }
        public string Barcode { get; set; }
        public string Description { get; set; }
        public decimal? ItemQty { get; set; }
        public decimal? Price { get; set; }
        public int? ItemId { get; set; }
        public int? CategoryId { get; set; }
        public int? ItemTypeId { get; set; }
        public decimal? ItemPrice { get; set; }
        public decimal? DiscountP { get; set; }
        public decimal? DiscountD { get; set; }
        public bool? Instruction { get; set; }
        public int? OrderId { get; set; }
        public decimal? Outstanding { get; set; }
        public int? TableItemId { get; set; }
        public decimal? DiscountPaidAmount { get; set; }
        public int? GroupId { get; set; }
        public bool? Selected { get; set; }
        public decimal? ToPay { get; set; }
        public decimal? LastPayment { get; set; }
    }
}
