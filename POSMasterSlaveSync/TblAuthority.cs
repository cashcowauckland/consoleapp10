﻿namespace Temp.Models
{
    public partial class TblAuthority
    {
        public int AuthorityId { get; set; }
        public string AuthorityName { get; set; }
    }
}
