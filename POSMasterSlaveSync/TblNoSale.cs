﻿namespace Temp.Models
{
    public partial class TblNoSale
    {
        public int NoSaleId { get; set; }
        public int? StaffId { get; set; }
        public string NoSaleDate { get; set; }
        public string NoSaleTime { get; set; }
    }
}
