﻿namespace Temp.Models
{
    public partial class TblFranchise
    {
        public int ShopId { get; set; }
        public string ShopCode { get; set; }
        public string ShopNames { get; set; }
        public string ShopPhone { get; set; }
        public string ShopFax { get; set; }
        public string ShopEmail { get; set; }
        public string ShopAddress { get; set; }
        public string ShopComment { get; set; }
        public string ShopIp { get; set; }
        public string DatabaseName { get; set; }
        public string LoginName { get; set; }
        public string Password { get; set; }
    }
}
