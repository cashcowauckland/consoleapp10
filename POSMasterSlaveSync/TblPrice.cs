﻿namespace Temp.Models
{
    public partial class TblPrice
    {
        public int PriceId { get; set; }
        public decimal? Amount { get; set; }
        public int? ItemId { get; set; }
        public int? Point { get; set; }
        public int? PriceLevel { get; set; }
    }
}
