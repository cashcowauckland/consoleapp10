﻿namespace Temp.Models
{
    public partial class TblRoleAuth
    {
        public int RoleAuthId { get; set; }
        public int? RoleId { get; set; }
        public int? AuthorityId { get; set; }
        public int? SetupId { get; set; }
    }
}
