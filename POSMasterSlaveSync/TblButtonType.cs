﻿namespace Temp.Models
{
    public partial class TblButtonType
    {
        public int ButtonTypeId { get; set; }
        public int? NumOfButton { get; set; }
    }
}
