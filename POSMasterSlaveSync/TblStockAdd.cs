﻿namespace Temp.Models
{
    public partial class TblStockAdd
    {
        public int StockAddId { get; set; }
        public string Date { get; set; }
        public int? ItemId { get; set; }
        public decimal? Qty { get; set; }
        public string Time { get; set; }
        public string StaffLogin { get; set; }
    }
}
