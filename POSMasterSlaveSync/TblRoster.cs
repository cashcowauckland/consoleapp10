﻿namespace Temp.Models
{
    public partial class TblRoster
    {
        public int RosterId { get; set; }
        public string Date { get; set; }
        public int? StaffId { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
    }
}
