﻿namespace POSMasterSlaveSync.Models
{
    public partial class TblTerminal
    {
        public int TerminalId { get; set; }
        public string TerminalIp { get; set; }
        public string MasterIp { get; set; }
        public int? ShopId { get; set; }
        public string LoginName { get; set; }
        public string Password { get; set; }
        public bool? EnableCashdraw { get; set; }
        public int PrinterId { get; set; }
        public bool? EnableEftpos { get; set; }
        public string EftposPort { get; set; }
        public string PoleCommand { get; set; }
        public string PolePort { get; set; }
        public string PoleText { get; set; }
        public string HqMsg { get; set; }
        public bool? ScaleManualEntry { get; set; }
        public bool? PrintBarcode { get; set; }
        public string ScalePort { get; set; }
        public bool? ChineseDisplay { get; set; }
        public string ButtonPressedDate { get; set; }
        public string ButtonPressedTime { get; set; }
        public bool? ComboDisplay { get; set; }
        public bool? DisplayKey { get; set; }
        public string EftposType { get; set; }
        public bool? UseExcel { get; set; }
        public bool? AgeRestricted { get; set; }
        public int? Age { get; set; }
        public int? InterfaceId { get; set; }
        public bool? ShowMargin { get; set; }
        public bool? PrintVoucher { get; set; }
        public int? PointToVoucher { get; set; }
        public decimal? VoucherValue { get; set; }
        public bool? ShowPriceKitchen { get; set; }
        public decimal? CommissionRate { get; set; }
        public int? ConnectionType { get; set; }
        public int? TableNo { get; set; }
        public string PoleBaudRate { get; set; }
        public string ScannerPort { get; set; }
        public string ScaleType { get; set; }
        public string CameraPort { get; set; }
        public bool? UseIpeftpos { get; set; }
        public bool? ShowDashboard { get; set; }
    }
}
