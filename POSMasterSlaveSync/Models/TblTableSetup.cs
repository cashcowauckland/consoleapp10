﻿namespace POSMasterSlaveSync.Models
{
    public partial class TblTableSetup
    {
        public int TableNum { get; set; }
        public int? CoordinateX { get; set; }
        public int? CoordinateY { get; set; }
        public int? Size { get; set; }
        public string Shape { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }
    }
}
