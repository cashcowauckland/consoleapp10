﻿namespace POSMasterSlaveSync.Models
{
    public partial class TblPosUpdate
    {
        public int PosUpdateId { get; set; }
        public string SqlQuery { get; set; }
        public string TableName { get; set; }
    }
}
