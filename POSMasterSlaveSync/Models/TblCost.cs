﻿namespace POSMasterSlaveSync.Models
{
    public partial class TblCost
    {
        public int CostId { get; set; }
        public string UpdateDate { get; set; }
        public decimal? Cost { get; set; }
        public int? ItemId { get; set; }
        public decimal? Stock { get; set; }
        public string ExpireDate { get; set; }
        public int? SupplierId { get; set; }
        public decimal? StockLeft { get; set; }
    }
}
