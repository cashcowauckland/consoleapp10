﻿namespace POSMasterSlaveSync.Models
{
    public partial class TblWarehouseQuery
    {
        public int QueryId { get; set; }
        public string Query { get; set; }
        public int? ItemId { get; set; }
    }
}
