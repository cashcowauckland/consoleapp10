﻿namespace POSMasterSlaveSync.Models
{
    public partial class TblInterface
    {
        public int InterfaceId { get; set; }
        public string InterfaceName { get; set; }
    }
}
