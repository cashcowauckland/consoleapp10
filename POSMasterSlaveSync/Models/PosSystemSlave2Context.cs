﻿using Microsoft.EntityFrameworkCore;

namespace POSMasterSlaveSync.Models
{
    public partial class PosSystemSlave2Context : DbContext
    {
        public PosSystemSlave2Context()
        {
        }

        public PosSystemSlave2Context(DbContextOptions<PosSystemSlave1Context> options)
            : base(options)
        {
        }

        public virtual DbSet<TblA4labelItem> TblA4labelItem { get; set; }
        public virtual DbSet<TblActivityLog> TblActivityLog { get; set; }
        public virtual DbSet<TblAuthority> TblAuthority { get; set; }
        public virtual DbSet<TblBarcode> TblBarcode { get; set; }
        public virtual DbSet<TblBlock> TblBlock { get; set; }
        public virtual DbSet<TblBlockPosition> TblBlockPosition { get; set; }
        public virtual DbSet<TblButtonType> TblButtonType { get; set; }
        public virtual DbSet<TblCashDeclaration> TblCashDeclaration { get; set; }
        public virtual DbSet<TblCategory> TblCategory { get; set; }
        public virtual DbSet<TblCheque> TblCheque { get; set; }
        public virtual DbSet<TblCommissionRange> TblCommissionRange { get; set; }
        public virtual DbSet<TblConnectionReport> TblConnectionReport { get; set; }
        public virtual DbSet<TblCost> TblCost { get; set; }
        public virtual DbSet<TblCostMargin> TblCostMargin { get; set; }
        public virtual DbSet<TblCourierPost> TblCourierPost { get; set; }
        public virtual DbSet<TblCustomer> TblCustomer { get; set; }
        public virtual DbSet<TblDailyRecord> TblDailyRecord { get; set; }
        public virtual DbSet<TblDisplaySetup> TblDisplaySetup { get; set; }
        public virtual DbSet<TblEftposReceipt> TblEftposReceipt { get; set; }
        public virtual DbSet<TblEftposTrans> TblEftposTrans { get; set; }
        public virtual DbSet<TblEmail> TblEmail { get; set; }
        public virtual DbSet<TblEmailServer> TblEmailServer { get; set; }
        public virtual DbSet<TblEthnic> TblEthnic { get; set; }
        public virtual DbSet<TblFranchise> TblFranchise { get; set; }
        public virtual DbSet<TblFunction> TblFunction { get; set; }
        public virtual DbSet<TblGroup> TblGroup { get; set; }
        public virtual DbSet<TblGroupDiscount> TblGroupDiscount { get; set; }
        public virtual DbSet<TblIndirect> TblIndirect { get; set; }
        public virtual DbSet<TblInterface> TblInterface { get; set; }
        public virtual DbSet<TblIpeftpos> TblIpeftpos { get; set; }
        public virtual DbSet<TblItem> TblItem { get; set; }
        public virtual DbSet<TblItemPrinter> TblItemPrinter { get; set; }
        public virtual DbSet<TblItemSubTab> TblItemSubTab { get; set; }
        public virtual DbSet<TblItemType> TblItemType { get; set; }
        public virtual DbSet<TblItemTypePosition> TblItemTypePosition { get; set; }
        public virtual DbSet<TblItemVolumePrice> TblItemVolumePrice { get; set; }
        public virtual DbSet<TblKitchenCategory> TblKitchenCategory { get; set; }
        public virtual DbSet<TblKitchenItem> TblKitchenItem { get; set; }
        public virtual DbSet<TblKitchenMonitor> TblKitchenMonitor { get; set; }
        public virtual DbSet<TblKitchenSetup> TblKitchenSetup { get; set; }
        public virtual DbSet<TblLabelOption> TblLabelOption { get; set; }
        public virtual DbSet<TblMainCategory> TblMainCategory { get; set; }
        public virtual DbSet<TblNoSale> TblNoSale { get; set; }
        public virtual DbSet<TblOrder> TblOrder { get; set; }
        public virtual DbSet<TblOrderItem> TblOrderItem { get; set; }
        public virtual DbSet<TblPayment> TblPayment { get; set; }
        public virtual DbSet<TblPaymentType> TblPaymentType { get; set; }
        public virtual DbSet<TblPending> TblPending { get; set; }
        public virtual DbSet<TblPosUpdate> TblPosUpdate { get; set; }
        public virtual DbSet<TblPrice> TblPrice { get; set; }
        public virtual DbSet<TblPriceItemType> TblPriceItemType { get; set; }
        public virtual DbSet<TblPrinter> TblPrinter { get; set; }
        public virtual DbSet<TblPrintStyles> TblPrintStyles { get; set; }
        public virtual DbSet<TblPromotion> TblPromotion { get; set; }
        public virtual DbSet<TblPromotionGroup> TblPromotionGroup { get; set; }
        public virtual DbSet<TblPromotionItem> TblPromotionItem { get; set; }
        public virtual DbSet<TblPromotionType> TblPromotionType { get; set; }
        public virtual DbSet<TblQuery> TblQuery { get; set; }
        public virtual DbSet<TblRole> TblRole { get; set; }
        public virtual DbSet<TblRoleAuth> TblRoleAuth { get; set; }
        public virtual DbSet<TblRoster> TblRoster { get; set; }
        public virtual DbSet<TblShift> TblShift { get; set; }
        public virtual DbSet<TblShop> TblShop { get; set; }
        public virtual DbSet<TblSlave> TblSlave { get; set; }
        public virtual DbSet<TblStaff> TblStaff { get; set; }
        public virtual DbSet<TblStampLevel> TblStampLevel { get; set; }
        public virtual DbSet<TblStockAdd> TblStockAdd { get; set; }
        public virtual DbSet<TblStockRequest> TblStockRequest { get; set; }
        public virtual DbSet<TblSubTab> TblSubTab { get; set; }
        public virtual DbSet<TblSupplier> TblSupplier { get; set; }
        public virtual DbSet<TblSupplierOrder> TblSupplierOrder { get; set; }
        public virtual DbSet<TblSupplierOrderItem> TblSupplierOrderItem { get; set; }
        public virtual DbSet<TblSupplierPrice> TblSupplierPrice { get; set; }
        public virtual DbSet<TblTab> TblTab { get; set; }
        public virtual DbSet<TblTable> TblTable { get; set; }
        public virtual DbSet<TblTablePayment> TblTablePayment { get; set; }
        public virtual DbSet<TblTableSetup> TblTableSetup { get; set; }
        public virtual DbSet<TblTableTran> TblTableTran { get; set; }
        public virtual DbSet<TblTerminal> TblTerminal { get; set; }
        public virtual DbSet<TblTranItem> TblTranItem { get; set; }
        public virtual DbSet<TblVoidItem> TblVoidItem { get; set; }
        public virtual DbSet<TblVoucher> TblVoucher { get; set; }
        public virtual DbSet<TblWarehouse> TblWarehouse { get; set; }
        public virtual DbSet<TblWarehouseLink> TblWarehouseLink { get; set; }
        public virtual DbSet<TblWarehouseQuery> TblWarehouseQuery { get; set; }
        public virtual DbSet<TblWeekDay> TblWeekDay { get; set; }

        // Unable to generate entity type for table 'dbo.tbl_A4Label'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tbl_TableItem'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tbl_Const_Setting'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tbl_Const_Value'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tbl_CourierBulk'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tbl_TextForum'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tbl_Voucher_Track'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tbl_Customer_Track'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tbl_Item_Track'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tbl_Label_Setup'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tbl_LoyaltyPrice'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tbl_OnlineSales'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tbl_PendingItem'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tbl_Activity_Type'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tbl_ReportEmail'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tbl_SaleType'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tbl_Server'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tbl_StockEmail'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.tbl_Sync'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=.\\sqlexpress;Database=POSSystemSlave2;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TblA4labelItem>(entity =>
            {
                entity.HasKey(e => e.LabelItemId);

                entity.ToTable("tbl_A4Label_Item");

                entity.Property(e => e.LabelItemId)
                    .HasColumnName("LabelItem_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.BarcodeHeight).HasColumnName("Barcode_Height");

                entity.Property(e => e.ItemName)
                    .HasColumnName("Item_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PositionX).HasColumnName("Position_X");

                entity.Property(e => e.PositionY).HasColumnName("Position_Y");
            });

            modelBuilder.Entity<TblActivityLog>(entity =>
            {
                entity.HasKey(e => e.ActivityId);

                entity.ToTable("tbl_Activity_Log");

                entity.Property(e => e.ActivityId).HasColumnName("Activity_ID");

                entity.Property(e => e.ActivityDate)
                    .HasColumnName("Activity_Date")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ActivityLog)
                    .HasColumnName("Activity_Log")
                    .HasMaxLength(1000);

                entity.Property(e => e.ActivityTime)
                    .HasColumnName("Activity_Time")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ActivityType).HasColumnName("Activity_Type");

                entity.Property(e => e.CustomerId).HasColumnName("Customer_ID");

                entity.Property(e => e.StaffId).HasColumnName("Staff_ID");

                entity.Property(e => e.TerminalId).HasColumnName("Terminal_ID");
            });

            modelBuilder.Entity<TblAuthority>(entity =>
            {
                entity.HasKey(e => e.AuthorityId);

                entity.ToTable("tbl_Authority");

                entity.Property(e => e.AuthorityId).HasColumnName("Authority_ID");

                entity.Property(e => e.AuthorityName)
                    .HasColumnName("Authority_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblBarcode>(entity =>
            {
                entity.HasKey(e => e.BarcodeId);

                entity.ToTable("tbl_Barcode");

                entity.Property(e => e.BarcodeId).HasColumnName("Barcode_ID");

                entity.Property(e => e.Barcode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ItemId).HasColumnName("Item_ID");
            });

            modelBuilder.Entity<TblBlock>(entity =>
            {
                entity.HasKey(e => e.BlockId);

                entity.ToTable("tbl_Block");

                entity.Property(e => e.BlockId).HasColumnName("Block_ID");

                entity.Property(e => e.BlockName)
                    .HasColumnName("Block_Name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.XCoordinate).HasColumnName("X_Coordinate");

                entity.Property(e => e.YCoordinate).HasColumnName("Y_Coordinate");
            });

            modelBuilder.Entity<TblBlockPosition>(entity =>
            {
                entity.HasKey(e => e.BlockNum);

                entity.ToTable("tbl_Block_Position");

                entity.Property(e => e.BlockNum)
                    .HasColumnName("Block_Num")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.BlockId).HasColumnName("Block_ID");
            });

            modelBuilder.Entity<TblButtonType>(entity =>
            {
                entity.HasKey(e => e.ButtonTypeId);

                entity.ToTable("tbl_Button_Type");

                entity.Property(e => e.ButtonTypeId).HasColumnName("Button_Type_ID");

                entity.Property(e => e.NumOfButton).HasColumnName("Num_Of_Button");
            });

            modelBuilder.Entity<TblCashDeclaration>(entity =>
            {
                entity.HasKey(e => e.DeclarationId);

                entity.ToTable("tbl_Cash_Declaration");

                entity.Property(e => e.DeclarationId).HasColumnName("Declaration_ID");

                entity.Property(e => e.Amount).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Date)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PaymentTypeId).HasColumnName("Payment_Type_ID");

                entity.Property(e => e.Time)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblCategory>(entity =>
            {
                entity.HasKey(e => e.CategoryId);

                entity.ToTable("tbl_Category");

                entity.Property(e => e.CategoryId)
                    .HasColumnName("Category_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AllowRedeem).HasColumnName("Allow_Redeem");

                entity.Property(e => e.CategoryName)
                    .HasColumnName("Category_Name")
                    .HasMaxLength(100);

                entity.Property(e => e.CommissionRate)
                    .HasColumnName("Commission_Rate")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.MainCategoryId).HasColumnName("MainCategory_ID");

                entity.Property(e => e.TaxRate)
                    .HasColumnName("Tax_Rate")
                    .HasColumnType("decimal(10, 2)");
            });

            modelBuilder.Entity<TblCheque>(entity =>
            {
                entity.HasKey(e => e.ChequeId);

                entity.ToTable("tbl_Cheque");

                entity.Property(e => e.ChequeId).HasColumnName("Cheque_ID");

                entity.Property(e => e.Bank)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Branch)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ContactDetail)
                    .HasColumnName("Contact_Detail")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Date)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Drawer)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Identification)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PaymentId).HasColumnName("Payment_ID");

                entity.Property(e => e.Total).HasColumnType("decimal(10, 2)");
            });

            modelBuilder.Entity<TblCommissionRange>(entity =>
            {
                entity.HasKey(e => e.RangeId);

                entity.ToTable("tbl_Commission_Range");

                entity.Property(e => e.RangeId).HasColumnName("Range_ID");

                entity.Property(e => e.CommissionRate)
                    .HasColumnName("Commission_Rate")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.SalesTo)
                    .HasColumnName("Sales_To")
                    .HasColumnType("decimal(10, 2)");
            });

            modelBuilder.Entity<TblConnectionReport>(entity =>
            {
                entity.HasKey(e => e.ConnectionReportId);

                entity.ToTable("tbl_Connection_Report");

                entity.Property(e => e.ConnectionReportId).HasColumnName("Connection_Report_ID");

                entity.Property(e => e.Date)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Time)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblCost>(entity =>
            {
                entity.HasKey(e => e.CostId);

                entity.ToTable("tbl_Cost");

                entity.Property(e => e.CostId).HasColumnName("Cost_ID");

                entity.Property(e => e.Cost).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.ExpireDate)
                    .HasColumnName("Expire_Date")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ItemId).HasColumnName("Item_ID");

                entity.Property(e => e.Stock).HasColumnType("decimal(10, 3)");

                entity.Property(e => e.StockLeft)
                    .HasColumnName("Stock_Left")
                    .HasColumnType("decimal(10, 3)");

                entity.Property(e => e.SupplierId).HasColumnName("Supplier_ID");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("Update_Date")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblCostMargin>(entity =>
            {
                entity.HasKey(e => e.MarginId);

                entity.ToTable("tbl_Cost_Margin");

                entity.Property(e => e.MarginId).HasColumnName("Margin_ID");

                entity.Property(e => e.CostTo)
                    .HasColumnName("Cost_To")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Price).HasColumnType("decimal(10, 2)");
            });

            modelBuilder.Entity<TblCourierPost>(entity =>
            {
                entity.HasKey(e => e.VanDoorId);

                entity.ToTable("tbl_CourierPost");

                entity.Property(e => e.VanDoorId).HasColumnName("VanDoor_ID");

                entity.Property(e => e.Total).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("Update_Date")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateTime)
                    .HasColumnName("Update_Time")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblCustomer>(entity =>
            {
                entity.HasKey(e => e.CustomerId);

                entity.ToTable("tbl_Customer");

                entity.Property(e => e.CustomerId).HasColumnName("Customer_ID");

                entity.Property(e => e.Barcode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreditAccount)
                    .HasColumnName("Credit_Account")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.CreditLeft)
                    .HasColumnName("Credit_Left")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.CreditLimit)
                    .HasColumnName("Credit_Limit")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.CustomerAddress)
                    .HasColumnName("Customer_Address")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerComment)
                    .HasColumnName("Customer_Comment")
                    .HasMaxLength(1000);

                entity.Property(e => e.CustomerDob)
                    .HasColumnName("Customer_DOB")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerEmail)
                    .HasColumnName("Customer_Email")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerFirstName)
                    .HasColumnName("Customer_First_Name")
                    .HasMaxLength(50);

                entity.Property(e => e.CustomerLastName)
                    .HasColumnName("Customer_Last_Name")
                    .HasMaxLength(50);

                entity.Property(e => e.CustomerMobile)
                    .HasColumnName("Customer_Mobile")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerPhone)
                    .HasColumnName("Customer_Phone")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerWork)
                    .HasColumnName("Customer_Work")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DiscountPercentage)
                    .HasColumnName("Discount_Percentage")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.FreeItem).HasColumnName("Free_Item");

                entity.Property(e => e.LaybyAmount)
                    .HasColumnName("Layby_Amount")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.PriceLevel).HasColumnName("Price_Level");

                entity.Property(e => e.StampLevel).HasColumnName("Stamp_Level");

                entity.Property(e => e.TotalPoint).HasColumnName("Total_Point");
            });

            modelBuilder.Entity<TblDailyRecord>(entity =>
            {
                entity.HasKey(e => e.DailyRecordId);

                entity.ToTable("tbl_DailyRecord");

                entity.Property(e => e.DailyRecordId).HasColumnName("DailyRecord_ID");

                entity.Property(e => e.Date)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NoSaleNum).HasColumnName("NoSale_Num");

                entity.Property(e => e.VoidEntireNum).HasColumnName("VoidEntire_Num");
            });

            modelBuilder.Entity<TblDisplaySetup>(entity =>
            {
                entity.HasKey(e => e.ControlId);

                entity.ToTable("tbl_Display_Setup");

                entity.Property(e => e.ControlId)
                    .HasColumnName("Control_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ControlType)
                    .HasColumnName("Control_Type")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CoordinateX).HasColumnName("Coordinate_X");

                entity.Property(e => e.CoordinateY).HasColumnName("Coordinate_Y");

                entity.Property(e => e.Font)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ImgPath)
                    .HasColumnName("Img_Path")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Style)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Text).HasMaxLength(500);
            });

            modelBuilder.Entity<TblEftposReceipt>(entity =>
            {
                entity.HasKey(e => e.EftposReceiptId);

                entity.ToTable("tbl_EftposReceipt");

                entity.Property(e => e.EftposReceiptId)
                    .HasColumnName("EftposReceiptID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.EftposTransId).HasColumnName("EftposTransID");

                entity.Property(e => e.MerchantId).HasColumnName("MerchantID");

                entity.Property(e => e.ReceiptText).IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblEftposTrans>(entity =>
            {
                entity.HasKey(e => e.EftposTransId);

                entity.ToTable("tbl_EftposTrans");

                entity.Property(e => e.EftposTransId)
                    .HasColumnName("EftposTransID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Cashout).HasColumnType("money");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.MerchantId).HasColumnName("MerchantID");

                entity.Property(e => e.OrderId).HasColumnName("OrderID");

                entity.Property(e => e.Purchase).HasColumnType("money");

                entity.Property(e => e.Refund).HasColumnType("money");

                entity.Property(e => e.TransType)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblEmail>(entity =>
            {
                entity.HasKey(e => e.EmailId);

                entity.ToTable("tbl_Email");

                entity.Property(e => e.EmailId).HasColumnName("Email_ID");

                entity.Property(e => e.Attachment)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Body)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.MailBcc)
                    .HasColumnName("Mail_BCC")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MailCc)
                    .HasColumnName("Mail_CC")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MailFrom)
                    .HasColumnName("Mail_From")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MailServer)
                    .HasColumnName("Mail_Server")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MailTo)
                    .HasColumnName("Mail_To")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Subject)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Time)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserName)
                    .HasColumnName("User_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WeekDay)
                    .HasColumnName("Week_Day")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblEmailServer>(entity =>
            {
                entity.HasKey(e => e.EmailServerId);

                entity.ToTable("tbl_EmailServer");

                entity.Property(e => e.EmailServerId).HasColumnName("EmailServer_ID");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Port)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Smtp)
                    .HasColumnName("SMTP")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ssl).HasColumnName("SSL");
            });

            modelBuilder.Entity<TblEthnic>(entity =>
            {
                entity.HasKey(e => e.EthnicId);

                entity.ToTable("tbl_Ethnic");

                entity.Property(e => e.EthnicId)
                    .HasColumnName("Ethnic_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Type)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblFranchise>(entity =>
            {
                entity.HasKey(e => e.ShopId);

                entity.ToTable("tbl_Franchise");

                entity.Property(e => e.ShopId)
                    .HasColumnName("Shop_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.DatabaseName)
                    .HasColumnName("Database_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LoginName)
                    .HasColumnName("Login_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ShopAddress)
                    .HasColumnName("Shop_Address")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ShopCode)
                    .HasColumnName("Shop_Code")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ShopComment)
                    .HasColumnName("Shop_Comment")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ShopEmail)
                    .HasColumnName("Shop_Email")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ShopFax)
                    .HasColumnName("Shop_Fax")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ShopIp)
                    .HasColumnName("Shop_IP")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ShopNames)
                    .HasColumnName("Shop_Names")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ShopPhone)
                    .HasColumnName("Shop_Phone")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblFunction>(entity =>
            {
                entity.HasKey(e => e.FunctionId);

                entity.ToTable("tbl_Function");

                entity.Property(e => e.FunctionId).HasColumnName("Function_ID");

                entity.Property(e => e.BackColor).HasColumnName("Back_Color");

                entity.Property(e => e.Description)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.FontSize).HasColumnName("Font_Size");

                entity.Property(e => e.ForeColor).HasColumnName("Fore_Color");

                entity.Property(e => e.FunctionName)
                    .HasColumnName("Function_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FunctionPosition).HasColumnName("Function_Position");

                entity.Property(e => e.FunctionText)
                    .HasColumnName("Function_Text")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.KeyBoardValue)
                    .HasColumnName("KeyBoard_Value")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PasswordProtect).HasColumnName("Password_Protect");
            });

            modelBuilder.Entity<TblGroup>(entity =>
            {
                entity.HasKey(e => e.GroupId);

                entity.ToTable("tbl_Group");

                entity.Property(e => e.GroupId).HasColumnName("Group_ID");

                entity.Property(e => e.GroupName)
                    .HasColumnName("Group_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblGroupDiscount>(entity =>
            {
                entity.HasKey(e => e.GroupDiscountId);

                entity.ToTable("tbl_GroupDiscount");

                entity.Property(e => e.GroupDiscountId).HasColumnName("GroupDiscount_ID");

                entity.Property(e => e.DiscountD)
                    .HasColumnName("Discount_D")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.DiscountP)
                    .HasColumnName("Discount_P")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.TerminalId).HasColumnName("Terminal_ID");
            });

            modelBuilder.Entity<TblIndirect>(entity =>
            {
                entity.HasKey(e => e.IndirectId);

                entity.ToTable("tbl_Indirect");

                entity.Property(e => e.IndirectId).HasColumnName("Indirect_ID");

                entity.Property(e => e.ChildId).HasColumnName("Child_ID");

                entity.Property(e => e.ItemId).HasColumnName("Item_ID");

                entity.Property(e => e.Qty).HasColumnType("decimal(10, 3)");

                entity.Property(e => e.Weight).HasColumnType("decimal(10, 2)");
            });

            modelBuilder.Entity<TblInterface>(entity =>
            {
                entity.HasKey(e => e.InterfaceId);

                entity.ToTable("tbl_Interface");

                entity.Property(e => e.InterfaceId)
                    .HasColumnName("Interface_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.InterfaceName)
                    .HasColumnName("Interface_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblIpeftpos>(entity =>
            {
                entity.HasKey(e => e.IpeftposId);

                entity.ToTable("tbl_IPEftpos");

                entity.Property(e => e.IpeftposId).HasColumnName("IPEftpos_ID");

                entity.Property(e => e.IpeftposIp)
                    .HasColumnName("IPEftpos_IP")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IpeftposName)
                    .HasColumnName("IPEftpos_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IpeftposPort)
                    .HasColumnName("IPEftpos_Port")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IsPrimary).HasColumnName("Is_Primary");
            });

            modelBuilder.Entity<TblItem>(entity =>
            {
                entity.HasKey(e => e.ItemId);

                entity.ToTable("tbl_Item");

                entity.Property(e => e.ItemId)
                    .HasColumnName("Item_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AddGst).HasColumnName("Add_GST");

                entity.Property(e => e.AgeRestricted).HasColumnName("Age_Restricted");

                entity.Property(e => e.Barcode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BoxQty)
                    .HasColumnName("Box_Qty")
                    .HasColumnType("decimal(10, 3)");

                entity.Property(e => e.CategoryId).HasColumnName("Category_ID");

                entity.Property(e => e.ChineseDesc)
                    .HasColumnName("Chinese_Desc")
                    .HasMaxLength(500);

                entity.Property(e => e.Comment).HasMaxLength(1000);

                entity.Property(e => e.CommentPopup).HasColumnName("Comment_Popup");

                entity.Property(e => e.CommissionRate)
                    .HasColumnName("Commission_Rate")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Cost).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.CurrentPoint).HasColumnName("Current_Point");

                entity.Property(e => e.CurrentPrice)
                    .HasColumnName("Current_Price")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.DisablePoint).HasColumnName("Disable_Point");

                entity.Property(e => e.ExpiredDate)
                    .HasColumnName("Expired_Date")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ImgPath)
                    .HasColumnName("Img_Path")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IsScaleItem).HasColumnName("Is_ScaleItem");

                entity.Property(e => e.ItemCode)
                    .HasColumnName("Item_Code")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ItemName)
                    .HasColumnName("Item_Name")
                    .HasMaxLength(500);

                entity.Property(e => e.LastStockQty)
                    .HasColumnName("LastStock_Qty")
                    .HasColumnType("decimal(10, 3)");

                entity.Property(e => e.LinkId).HasColumnName("Link_ID");

                entity.Property(e => e.NonAccumlating).HasColumnName("Non_Accumlating");

                entity.Property(e => e.NonDiscount).HasColumnName("Non_Discount");

                entity.Property(e => e.PriceLevelPoint).HasColumnName("Price_Level_Point");

                entity.Property(e => e.RecmdQty).HasColumnName("Recmd_Qty");

                entity.Property(e => e.RemainQty)
                    .HasColumnName("Remain_Qty")
                    .HasColumnType("decimal(10, 3)");

                entity.Property(e => e.ScaleRange).HasColumnName("Scale_Range");

                entity.Property(e => e.ScaleRangeMax)
                    .HasColumnName("Scale_RangeMax")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.ScaleRangeMin)
                    .HasColumnName("Scale_RangeMin")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.ScaleUnit)
                    .HasColumnName("Scale_Unit")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SpecialPrice)
                    .HasColumnName("Special_Price")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.StockLevel).HasColumnName("Stock_Level");

                entity.Property(e => e.TotalStock)
                    .HasColumnName("Total_Stock")
                    .HasColumnType("decimal(10, 3)");

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("Update_Date")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UseIndirectPrice).HasColumnName("Use_Indirect_Price");

                entity.Property(e => e.WarehouseId).HasColumnName("Warehouse_ID");

                entity.Property(e => e.WebItemId).HasColumnName("Web_Item_ID");
            });

            modelBuilder.Entity<TblItemPrinter>(entity =>
            {
                entity.HasKey(e => e.ItemPrinterId);

                entity.ToTable("tbl_Item_Printer");

                entity.Property(e => e.ItemPrinterId).HasColumnName("Item_Printer_ID");

                entity.Property(e => e.CheckedStatus).HasColumnName("Checked_Status");

                entity.Property(e => e.ItemId).HasColumnName("Item_ID");

                entity.Property(e => e.PrinterId).HasColumnName("Printer_ID");
            });

            modelBuilder.Entity<TblItemSubTab>(entity =>
            {
                entity.HasKey(e => e.ItemSubTabId);

                entity.ToTable("tbl_Item_SubTab");

                entity.Property(e => e.ItemSubTabId).HasColumnName("ItemSubTab_ID");

                entity.Property(e => e.FontColor).HasColumnName("Font_Color");

                entity.Property(e => e.FontSize).HasColumnName("Font_Size");

                entity.Property(e => e.ItemId).HasColumnName("Item_ID");

                entity.Property(e => e.PositionNum).HasColumnName("Position_Num");

                entity.Property(e => e.SubTabId).HasColumnName("SubTab_ID");

                entity.HasOne(d => d.Item)
                    .WithMany(p => p.TblItemSubTab)
                    .HasForeignKey(d => d.ItemId)
                    .HasConstraintName("FK_tbl_Item_SubTab_tbl_Item");
            });

            modelBuilder.Entity<TblItemType>(entity =>
            {
                entity.HasKey(e => e.ItemTypeId);

                entity.ToTable("tbl_ItemType");

                entity.Property(e => e.ItemTypeId).HasColumnName("ItemType_ID");

                entity.Property(e => e.GroupName)
                    .HasColumnName("Group_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TypeName)
                    .HasColumnName("Type_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblItemTypePosition>(entity =>
            {
                entity.HasKey(e => e.ItemPositionId);

                entity.ToTable("tbl_ItemType_Position");

                entity.Property(e => e.ItemPositionId)
                    .HasColumnName("ItemPosition_ID")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.ItemId).HasColumnName("Item_ID");

                entity.Property(e => e.ItemTypeId).HasColumnName("ItemType_ID");

                entity.HasOne(d => d.ItemPosition)
                    .WithOne(p => p.TblItemTypePosition)
                    .HasForeignKey<TblItemTypePosition>(d => d.ItemPositionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tbl_ItemType_Position_tbl_ItemType");
            });

            modelBuilder.Entity<TblItemVolumePrice>(entity =>
            {
                entity.HasKey(e => e.ItemVolumePriceId);

                entity.ToTable("tbl_Item_Volume_Price");

                entity.Property(e => e.ItemVolumePriceId).HasColumnName("Item_Volume_Price_ID");

                entity.Property(e => e.ItemId).HasColumnName("Item_ID");

                entity.Property(e => e.Price).HasColumnType("decimal(10, 3)");

                entity.Property(e => e.VolumeTo).HasColumnName("Volume_To");
            });

            modelBuilder.Entity<TblKitchenCategory>(entity =>
            {
                entity.HasKey(e => new { e.SetupId, e.CategoryId });

                entity.ToTable("tbl_Kitchen_Category");

                entity.Property(e => e.SetupId).HasColumnName("Setup_ID");

                entity.Property(e => e.CategoryId).HasColumnName("Category_ID");
            });

            modelBuilder.Entity<TblKitchenItem>(entity =>
            {
                entity.HasKey(e => e.KitchenItemId);

                entity.ToTable("tbl_KitchenItem");

                entity.Property(e => e.KitchenItemId).HasColumnName("KitchenItem_ID");

                entity.Property(e => e.CategoryId).HasColumnName("Category_ID");

                entity.Property(e => e.Desc2)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.ItemName)
                    .IsRequired()
                    .HasColumnName("Item_Name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ItemStatus).HasColumnName("Item_Status");

                entity.Property(e => e.OrderDate)
                    .IsRequired()
                    .HasColumnName("Order_Date")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OrderId).HasColumnName("Order_ID");

                entity.Property(e => e.OrderStatus).HasColumnName("Order_Status");

                entity.Property(e => e.OrderTime)
                    .IsRequired()
                    .HasColumnName("Order_Time")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Qty).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.SequenceId).HasColumnName("Sequence_ID");

                entity.Property(e => e.ServiceSameTime).HasColumnName("Service_Same_Time");

                entity.Property(e => e.StaffId).HasColumnName("Staff_ID");

                entity.Property(e => e.TerminalId).HasColumnName("Terminal_ID");
            });

            modelBuilder.Entity<TblKitchenMonitor>(entity =>
            {
                entity.HasKey(e => new { e.SetupId, e.KitchenItemId });

                entity.ToTable("tbl_Kitchen_Monitor");

                entity.Property(e => e.SetupId).HasColumnName("Setup_ID");

                entity.Property(e => e.KitchenItemId).HasColumnName("KitchenItem_ID");

                entity.Property(e => e.OrderId).HasColumnName("Order_ID");

                entity.Property(e => e.OrderStatus).HasColumnName("Order_Status");
            });

            modelBuilder.Entity<TblKitchenSetup>(entity =>
            {
                entity.HasKey(e => e.SetupId);

                entity.ToTable("tbl_KitchenSetup");

                entity.Property(e => e.SetupId).HasColumnName("Setup_ID");

                entity.Property(e => e.IsCategory).HasColumnName("Is_Category");

                entity.Property(e => e.IsPrintReceipt).HasColumnName("Is_Print_Receipt");

                entity.Property(e => e.LangOpt).HasColumnName("lang_opt");

                entity.Property(e => e.LoginUserName)
                    .HasColumnName("Login_User_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NumOfDisplay).HasColumnName("Num_Of_Display");

                entity.Property(e => e.TimeOutMinutes).HasColumnName("Time_Out_Minutes");
            });

            modelBuilder.Entity<TblLabelOption>(entity =>
            {
                entity.HasKey(e => e.LabelOptionId);

                entity.ToTable("tbl_Label_Option");

                entity.Property(e => e.LabelOptionId).HasColumnName("Label_Option_ID");

                entity.Property(e => e.CodePrintSetId).HasColumnName("Code_Print_SetID");

                entity.Property(e => e.Desc2PrintSetId).HasColumnName("Desc2_Print_SetID");

                entity.Property(e => e.DescPrintSetId).HasColumnName("Desc_Print_SetID");

                entity.Property(e => e.LabelOptionName)
                    .HasColumnName("Label_Option_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PricePrintSetId).HasColumnName("Price_Print_SetID");

                entity.Property(e => e.PrintBarcode).HasColumnName("Print_Barcode");
            });

            modelBuilder.Entity<TblMainCategory>(entity =>
            {
                entity.HasKey(e => e.MainCategoryId);

                entity.ToTable("tbl_MainCategory");

                entity.Property(e => e.MainCategoryId)
                    .HasColumnName("MainCategory_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.MainCategoryName)
                    .HasColumnName("MainCategory_Name")
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<TblNoSale>(entity =>
            {
                entity.HasKey(e => e.NoSaleId);

                entity.ToTable("tbl_NoSale");

                entity.Property(e => e.NoSaleId).HasColumnName("NoSale_ID");

                entity.Property(e => e.NoSaleDate)
                    .HasColumnName("NoSale_Date")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NoSaleTime)
                    .HasColumnName("NoSale_Time")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StaffId).HasColumnName("Staff_ID");
            });

            modelBuilder.Entity<TblOrder>(entity =>
            {
                entity.HasKey(e => e.OrderId);

                entity.ToTable("tbl_Order");

                entity.Property(e => e.OrderId).HasColumnName("Order_ID");

                entity.Property(e => e.Barcode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CashoutAmount)
                    .HasColumnName("Cashout_Amount")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Change)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Comment)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerId).HasColumnName("Customer_ID");

                entity.Property(e => e.DiscountD)
                    .HasColumnName("Discount_D")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.DiscountP)
                    .HasColumnName("Discount_P")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.DiscountPaidAmount).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.DueDate)
                    .HasColumnName("Due_Date")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EthnicId)
                    .HasColumnName("Ethnic_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OrderDate)
                    .HasColumnName("Order_Date")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OrderTime)
                    .HasColumnName("Order_Time")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SaleStaffId).HasColumnName("SaleStaff_ID");

                entity.Property(e => e.SaleTypeId).HasColumnName("SaleType_ID");

                entity.Property(e => e.StaffId).HasColumnName("Staff_ID");

                entity.Property(e => e.SurchargeP)
                    .HasColumnName("Surcharge_P")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.TagNum).HasColumnName("Tag_Num");

                entity.Property(e => e.TaxContent)
                    .HasColumnName("Tax_Content")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Tender)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TerminalId).HasColumnName("Terminal_ID");

                entity.Property(e => e.TotalSales)
                    .HasColumnName("Total_Sales")
                    .HasColumnType("decimal(10, 2)");
            });

            modelBuilder.Entity<TblOrderItem>(entity =>
            {
                entity.HasKey(e => e.OrderItemId);

                entity.ToTable("tbl_OrderItem");

                entity.Property(e => e.OrderItemId).HasColumnName("OrderItem_ID");

                entity.Property(e => e.CommissionRate)
                    .HasColumnName("Commission_Rate")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Cost).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.DiscountD)
                    .HasColumnName("Discount_D")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.DiscountP)
                    .HasColumnName("Discount_P")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.GroupDiscountId).HasColumnName("GroupDiscount_ID");

                entity.Property(e => e.ItemId).HasColumnName("Item_ID");

                entity.Property(e => e.ItemPrice)
                    .HasColumnName("Item_Price")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.ItemQty)
                    .HasColumnName("item_Qty")
                    .HasColumnType("decimal(10, 3)");

                entity.Property(e => e.ItemTypeId).HasColumnName("ItemType_ID");

                entity.Property(e => e.NonDiscountable).HasColumnName("Non_Discountable");

                entity.Property(e => e.OrderId).HasColumnName("Order_ID");

                entity.Property(e => e.TerminalId).HasColumnName("Terminal_ID");

                entity.Property(e => e.TrackBarcode)
                    .HasColumnName("Track_Barcode")
                    .HasMaxLength(1000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblPayment>(entity =>
            {
                entity.HasKey(e => e.PaymentId);

                entity.ToTable("tbl_Payment");

                entity.Property(e => e.PaymentId).HasColumnName("Payment_ID");

                entity.Property(e => e.Amount).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.AmountOwe)
                    .HasColumnName("Amount_Owe")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.IsCustomer).HasDefaultValueSql("((0))");

                entity.Property(e => e.OrderId).HasColumnName("Order_ID");

                entity.Property(e => e.PaymentDate)
                    .HasColumnName("Payment_Date")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PaymentTypeId).HasColumnName("Payment_Type_ID");

                entity.Property(e => e.TerminalId).HasColumnName("Terminal_ID");
            });

            modelBuilder.Entity<TblPaymentType>(entity =>
            {
                entity.HasKey(e => e.PaymentTypeId);

                entity.ToTable("tbl_Payment_Type");

                entity.Property(e => e.PaymentTypeId).HasColumnName("Payment_Type_ID");

                entity.Property(e => e.HidDisplay).HasColumnName("Hid_Display");

                entity.Property(e => e.OpenCashdraw).HasColumnName("Open_Cashdraw");

                entity.Property(e => e.PaymentTypeName)
                    .HasColumnName("Payment_Type_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UseEftpos).HasColumnName("Use_Eftpos");
            });

            modelBuilder.Entity<TblPending>(entity =>
            {
                entity.HasKey(e => e.PendingId);

                entity.ToTable("tbl_Pending");

                entity.Property(e => e.PendingId)
                    .HasColumnName("Pending_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CustomerId)
                    .HasColumnName("Customer_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DateTime)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Ref)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SaveDate)
                    .HasColumnName("Save_Date")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblPosUpdate>(entity =>
            {
                entity.HasKey(e => e.PosUpdateId);

                entity.ToTable("tbl_Pos_Update");

                entity.Property(e => e.PosUpdateId).HasColumnName("Pos_Update_ID");

                entity.Property(e => e.SqlQuery)
                    .HasColumnName("Sql_Query")
                    .HasMaxLength(1000);

                entity.Property(e => e.TableName)
                    .HasColumnName("Table_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblPrice>(entity =>
            {
                entity.HasKey(e => e.PriceId);

                entity.ToTable("tbl_Price");

                entity.Property(e => e.PriceId).HasColumnName("Price_ID");

                entity.Property(e => e.Amount).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.ItemId).HasColumnName("Item_ID");

                entity.Property(e => e.PriceLevel).HasColumnName("Price_Level");
            });

            modelBuilder.Entity<TblPriceItemType>(entity =>
            {
                entity.HasKey(e => e.PriceId);

                entity.ToTable("tbl_Price_ItemType");

                entity.Property(e => e.PriceId).HasColumnName("Price_ID");

                entity.Property(e => e.Amount).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.ItemPositionId).HasColumnName("ItemPosition_ID");

                entity.Property(e => e.PriceLevel).HasColumnName("Price_Level");
            });

            modelBuilder.Entity<TblPrinter>(entity =>
            {
                entity.HasKey(e => e.PrinterId);

                entity.ToTable("tbl_Printer");

                entity.Property(e => e.PrinterId).HasColumnName("Printer_ID");

                entity.Property(e => e.Alignment)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FontSize)
                    .HasColumnName("Font_Size")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FontType)
                    .HasColumnName("Font_Type")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FooterMessage)
                    .HasColumnName("Footer_Message")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.HeaderMessage)
                    .HasColumnName("Header_Message")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.Language)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrinterModel)
                    .HasColumnName("Printer_Model")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrinterName)
                    .HasColumnName("Printer_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SelectedPort)
                    .HasColumnName("Selected_Port")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Setting)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ShopId).HasColumnName("Shop_ID");

                entity.Property(e => e.SubHeaderMessage)
                    .HasColumnName("SubHeader_Message")
                    .HasMaxLength(1000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblPrintStyles>(entity =>
            {
                entity.HasKey(e => e.PrintSettingId);

                entity.ToTable("tbl_Print_Styles");

                entity.Property(e => e.PrintSettingId).HasColumnName("Print_Setting_ID");

                entity.Property(e => e.Alignment)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FontSize)
                    .HasColumnName("Font_Size")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FontType)
                    .HasColumnName("Font_Type")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrintSettingName)
                    .HasColumnName("Print_Setting_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblPromotion>(entity =>
            {
                entity.HasKey(e => e.PromotionId);

                entity.ToTable("tbl_Promotion");

                entity.Property(e => e.PromotionId).HasColumnName("Promotion_ID");

                entity.Property(e => e.DateFrom)
                    .HasColumnName("Date_From")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DateTo)
                    .HasColumnName("Date_To")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DiscountP)
                    .HasColumnName("Discount_P")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.MultipleGroup).HasColumnName("Multiple_Group");

                entity.Property(e => e.PromotionTypeId).HasColumnName("PromotionType_ID");

                entity.Property(e => e.QtyBuy).HasColumnName("Qty_Buy");

                entity.Property(e => e.QtyGet).HasColumnName("Qty_Get");
            });

            modelBuilder.Entity<TblPromotionGroup>(entity =>
            {
                entity.HasKey(e => e.PromotionGroupId);

                entity.ToTable("tbl_PromotionGroup");

                entity.Property(e => e.PromotionGroupId).HasColumnName("PromotionGroup_ID");

                entity.Property(e => e.GroupNo).HasColumnName("Group_No");

                entity.Property(e => e.PromotionId).HasColumnName("Promotion_ID");

                entity.Property(e => e.QtyBuyGet).HasColumnName("Qty_BuyGet");
            });

            modelBuilder.Entity<TblPromotionItem>(entity =>
            {
                entity.HasKey(e => e.ProItemId);

                entity.ToTable("tbl_Promotion_Item");

                entity.Property(e => e.ProItemId).HasColumnName("ProItem_ID");

                entity.Property(e => e.GroupId).HasColumnName("Group_ID");

                entity.Property(e => e.ItemId).HasColumnName("Item_ID");

                entity.Property(e => e.PromotionGroupId).HasColumnName("PromotionGroup_ID");

                entity.Property(e => e.PromotionId).HasColumnName("Promotion_ID");
            });

            modelBuilder.Entity<TblPromotionType>(entity =>
            {
                entity.HasKey(e => e.PromotionTypeId);

                entity.ToTable("tbl_PromotionType");

                entity.Property(e => e.PromotionTypeId)
                    .HasColumnName("PromotionType_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.PromotionTypeName)
                    .HasColumnName("PromotionType_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblQuery>(entity =>
            {
                entity.HasKey(e => e.QueryId);

                entity.ToTable("tbl_Query");

                entity.Property(e => e.QueryId).HasColumnName("Query_ID");

                entity.Property(e => e.SqlQuery)
                    .HasColumnName("Sql_Query")
                    .HasColumnType("ntext");
            });

            modelBuilder.Entity<TblRole>(entity =>
            {
                entity.HasKey(e => e.RoleId);

                entity.ToTable("tbl_Role");

                entity.Property(e => e.RoleId).HasColumnName("Role_ID");

                entity.Property(e => e.RoleName)
                    .HasColumnName("Role_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblRoleAuth>(entity =>
            {
                entity.HasKey(e => e.RoleAuthId);

                entity.ToTable("tbl_Role_Auth");

                entity.Property(e => e.RoleAuthId).HasColumnName("Role_Auth_ID");

                entity.Property(e => e.AuthorityId).HasColumnName("Authority_ID");

                entity.Property(e => e.RoleId).HasColumnName("Role_ID");

                entity.Property(e => e.SetupId).HasColumnName("Setup_ID");
            });

            modelBuilder.Entity<TblRoster>(entity =>
            {
                entity.HasKey(e => e.RosterId);

                entity.ToTable("tbl_Roster");

                entity.Property(e => e.RosterId).HasColumnName("Roster_ID");

                entity.Property(e => e.Date)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EndTime)
                    .HasColumnName("End_Time")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StaffId).HasColumnName("Staff_ID");

                entity.Property(e => e.StartTime)
                    .HasColumnName("Start_Time")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblShift>(entity =>
            {
                entity.HasKey(e => e.ShiftId);

                entity.ToTable("tbl_Shift");

                entity.Property(e => e.ShiftId).HasColumnName("Shift_ID");

                entity.Property(e => e.EndShiftDate)
                    .HasColumnName("End_Shift_Date")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EndTime)
                    .HasColumnName("End_Time")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PayRate)
                    .HasColumnName("Pay_Rate")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.ShiftDate)
                    .HasColumnName("Shift_Date")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StaffId).HasColumnName("Staff_ID");

                entity.Property(e => e.StartTime)
                    .HasColumnName("Start_Time")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UploadDate)
                    .HasColumnName("Upload_Date")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblShop>(entity =>
            {
                entity.HasKey(e => e.ShopId);

                entity.ToTable("tbl_Shop");

                entity.Property(e => e.ShopId)
                    .HasColumnName("Shop_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AcctName)
                    .HasColumnName("Acct_Name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AcctNo)
                    .HasColumnName("Acct_No")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ButtonPressedDate)
                    .HasColumnName("Button_Pressed_Date")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ButtonPressedTime)
                    .HasColumnName("Button_Pressed_Time")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ButtonTypeId).HasColumnName("Button_Type_ID");

                entity.Property(e => e.CurrencyName)
                    .HasColumnName("Currency_Name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CurrencySymbol)
                    .HasColumnName("Currency_Symbol")
                    .HasMaxLength(50);

                entity.Property(e => e.Data)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.GstName)
                    .HasColumnName("GST_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GstNo)
                    .HasColumnName("GST_No")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GstRate)
                    .HasColumnName("GST_Rate")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.InvoiceDetails).HasColumnName("Invoice_Details");

                entity.Property(e => e.InvoiceNo).HasColumnName("Invoice_No");

                entity.Property(e => e.LastStockTake)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNo)
                    .HasColumnName("Phone_No")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PlimusId)
                    .HasColumnName("Plimus_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PointPerDollar).HasColumnName("Point_PerDollar");

                entity.Property(e => e.PointValue)
                    .HasColumnName("Point_Value")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Ref)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ShopAddress)
                    .HasColumnName("Shop_Address")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ShopName)
                    .HasColumnName("Shop_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ShowItemSummary).HasColumnName("Show_ItemSummary");

                entity.Property(e => e.ShowPaySummary).HasColumnName("Show_PaySummary");

                entity.Property(e => e.SlaveUpdate).HasColumnName("Slave_Update");

                entity.Property(e => e.TemplateText)
                    .HasColumnName("Template_Text")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate)
                    .HasColumnName("Update_Date")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Version)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblSlave>(entity =>
            {
                entity.HasKey(e => e.SlaveId);

                entity.ToTable("tbl_Slave");

                entity.Property(e => e.SlaveId).HasColumnName("Slave_ID");

                entity.Property(e => e.DatabaseName)
                    .HasColumnName("Database_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Login)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MainDatabase)
                    .HasColumnName("Main_Database")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PosUpdateId).HasColumnName("Pos_Update_ID");

                entity.Property(e => e.QueryId).HasColumnName("Query_ID");

                entity.Property(e => e.SlaveIp)
                    .HasColumnName("Slave_IP")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblStaff>(entity =>
            {
                entity.HasKey(e => e.StaffId);

                entity.ToTable("tbl_Staff");

                entity.Property(e => e.StaffId).HasColumnName("Staff_ID");

                entity.Property(e => e.PayRate)
                    .HasColumnName("Pay_Rate")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.PriceLevel).HasColumnName("Price_Level");

                entity.Property(e => e.RoleId).HasColumnName("Role_ID");

                entity.Property(e => e.StaffBarcode)
                    .HasColumnName("Staff_Barcode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StaffFirstName)
                    .HasColumnName("Staff_First_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StaffLastName)
                    .HasColumnName("Staff_Last_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StaffLogin)
                    .HasColumnName("Staff_Login")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StaffPassword)
                    .HasColumnName("Staff_password")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblStampLevel>(entity =>
            {
                entity.HasKey(e => e.StampId);

                entity.ToTable("tbl_Stamp_Level");

                entity.Property(e => e.StampId).HasColumnName("Stamp_ID");

                entity.Property(e => e.StampLevel).HasColumnName("Stamp_Level");
            });

            modelBuilder.Entity<TblStockAdd>(entity =>
            {
                entity.HasKey(e => e.StockAddId);

                entity.ToTable("tbl_StockAdd");

                entity.Property(e => e.StockAddId).HasColumnName("StockAdd_ID");

                entity.Property(e => e.Date)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ItemId).HasColumnName("Item_ID");

                entity.Property(e => e.Qty).HasColumnType("decimal(10, 3)");

                entity.Property(e => e.StaffLogin)
                    .HasColumnName("Staff_Login")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Time)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblStockRequest>(entity =>
            {
                entity.HasKey(e => e.RequestId);

                entity.ToTable("tbl_StockRequest");

                entity.Property(e => e.RequestId).HasColumnName("Request_ID");

                entity.Property(e => e.AckQty)
                    .HasColumnName("Ack_Qty")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Comment)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Date)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DispachQty)
                    .HasColumnName("Dispach_Qty")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.HqComment)
                    .HasColumnName("HQ_Comment")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ItemId).HasColumnName("Item_ID");

                entity.Property(e => e.OrigRequestId).HasColumnName("Orig_Request_ID");

                entity.Property(e => e.Qty).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Result)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ShopCode)
                    .HasColumnName("Shop_Code")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblSubTab>(entity =>
            {
                entity.HasKey(e => e.SubTabId);

                entity.ToTable("tbl_SubTab");

                entity.Property(e => e.SubTabId).HasColumnName("SubTab_ID");

                entity.Property(e => e.PositionNum).HasColumnName("Position_Num");

                entity.Property(e => e.SubTabName)
                    .HasColumnName("SubTab_Name")
                    .HasMaxLength(50);

                entity.Property(e => e.TabId).HasColumnName("Tab_ID");
            });

            modelBuilder.Entity<TblSupplier>(entity =>
            {
                entity.HasKey(e => e.SupplierId);

                entity.ToTable("tbl_Supplier");

                entity.Property(e => e.SupplierId).HasColumnName("Supplier_ID");

                entity.Property(e => e.Discount).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Fax)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SupplierAddress)
                    .HasColumnName("Supplier_Address")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.SupplierCode)
                    .HasColumnName("Supplier_Code")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SupplierComment)
                    .HasColumnName("Supplier_Comment")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SupplierEmail)
                    .HasColumnName("Supplier_Email")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SupplierName)
                    .HasColumnName("Supplier_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SupplierPhone)
                    .HasColumnName("Supplier_Phone")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblSupplierOrder>(entity =>
            {
                entity.HasKey(e => e.SupplierOrderId);

                entity.ToTable("tbl_Supplier_Order");

                entity.Property(e => e.SupplierOrderId).HasColumnName("Supplier_Order_ID");

                entity.Property(e => e.Comment)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Date)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InvoiceNo)
                    .HasColumnName("Invoice_No")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SupplierId).HasColumnName("Supplier_ID");
            });

            modelBuilder.Entity<TblSupplierOrderItem>(entity =>
            {
                entity.HasKey(e => e.SupplierOrderItemId);

                entity.ToTable("tbl_Supplier_OrderItem");

                entity.Property(e => e.SupplierOrderItemId).HasColumnName("Supplier_OrderItem_ID");

                entity.Property(e => e.AckQty).HasColumnName("Ack_Qty");

                entity.Property(e => e.Comment)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CommentOrder)
                    .HasColumnName("Comment_Order")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ItemId).HasColumnName("Item_ID");

                entity.Property(e => e.Price).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.SupplierOrderId).HasColumnName("Supplier_Order_ID");
            });

            modelBuilder.Entity<TblSupplierPrice>(entity =>
            {
                entity.HasKey(e => e.SupplierPriceId);

                entity.ToTable("tbl_Supplier_Price");

                entity.Property(e => e.SupplierPriceId).HasColumnName("Supplier_Price_ID");

                entity.Property(e => e.ItemId).HasColumnName("Item_ID");

                entity.Property(e => e.Price).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.SupplierId).HasColumnName("Supplier_ID");

                entity.Property(e => e.SupplierItemBarcode)
                    .HasColumnName("Supplier_Item_Barcode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SupplierItemCode)
                    .HasColumnName("Supplier_Item_Code")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SupplierItemName)
                    .HasColumnName("Supplier_Item_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblTab>(entity =>
            {
                entity.HasKey(e => e.TabId);

                entity.ToTable("tbl_Tab");

                entity.Property(e => e.TabId).HasColumnName("Tab_ID");

                entity.Property(e => e.PositionNum).HasColumnName("Position_Num");

                entity.Property(e => e.TabName)
                    .HasColumnName("Tab_Name")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TblTable>(entity =>
            {
                entity.HasKey(e => e.TableId);

                entity.ToTable("tbl_Table");

                entity.Property(e => e.TableId).HasColumnName("Table_ID");

                entity.Property(e => e.CoordinateX).HasColumnName("Coordinate_X");

                entity.Property(e => e.CoordinateY).HasColumnName("Coordinate_Y");

                entity.Property(e => e.OrderId).HasColumnName("Order_ID");

                entity.Property(e => e.ReserveId).HasColumnName("Reserve_ID");

                entity.Property(e => e.Shape)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StaffId).HasColumnName("Staff_ID");

                entity.Property(e => e.TableNum).HasColumnName("Table_Num");

                entity.Property(e => e.TableStatusId).HasColumnName("TableStatus_ID");
            });

            modelBuilder.Entity<TblTablePayment>(entity =>
            {
                entity.HasKey(e => e.PaymentId);

                entity.ToTable("tbl_TablePayment");

                entity.Property(e => e.PaymentId).HasColumnName("Payment_ID");

                entity.Property(e => e.Barcode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CategoryId).HasColumnName("Category_ID");

                entity.Property(e => e.Description).HasMaxLength(50);

                entity.Property(e => e.DiscountD)
                    .HasColumnName("Discount_D")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.DiscountP)
                    .HasColumnName("Discount_P")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.DiscountPaidAmount).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.GroupId).HasColumnName("Group_ID");

                entity.Property(e => e.ItemId).HasColumnName("Item_ID");

                entity.Property(e => e.ItemPrice)
                    .HasColumnName("Item_Price")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.ItemQty)
                    .HasColumnName("Item_Qty")
                    .HasColumnType("decimal(10, 3)");

                entity.Property(e => e.ItemTypeId).HasColumnName("ItemType_ID");

                entity.Property(e => e.LastPayment).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.OrderId).HasColumnName("Order_ID");

                entity.Property(e => e.Outstanding).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Price).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.TableItemId).HasColumnName("TableItem_ID");

                entity.Property(e => e.ToPay).HasColumnType("decimal(10, 2)");
            });

            modelBuilder.Entity<TblTableSetup>(entity =>
            {
                entity.HasKey(e => e.TableNum);

                entity.ToTable("tbl_TableSetup");

                entity.Property(e => e.TableNum)
                    .HasColumnName("Table_Num")
                    .ValueGeneratedNever();

                entity.Property(e => e.CoordinateX).HasColumnName("Coordinate_X");

                entity.Property(e => e.CoordinateY).HasColumnName("Coordinate_Y");

                entity.Property(e => e.Description)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Shape)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblTableTran>(entity =>
            {
                entity.HasKey(e => e.TranId);

                entity.ToTable("tbl_TableTran");

                entity.Property(e => e.TranId).HasColumnName("Tran_ID");

                entity.Property(e => e.OrderId).HasColumnName("Order_ID");
            });

            modelBuilder.Entity<TblTerminal>(entity =>
            {
                entity.HasKey(e => e.TerminalId);

                entity.ToTable("tbl_Terminal");

                entity.Property(e => e.TerminalId)
                    .HasColumnName("Terminal_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.AgeRestricted).HasColumnName("Age_Restricted");

                entity.Property(e => e.ButtonPressedDate)
                    .HasColumnName("Button_Pressed_Date")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ButtonPressedTime)
                    .HasColumnName("Button_Pressed_Time")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CameraPort)
                    .HasColumnName("Camera_Port")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ChineseDisplay).HasColumnName("Chinese_Display");

                entity.Property(e => e.ComboDisplay).HasColumnName("Combo_Display");

                entity.Property(e => e.CommissionRate)
                    .HasColumnName("Commission_Rate")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.ConnectionType).HasColumnName("Connection_Type");

                entity.Property(e => e.DisplayKey).HasColumnName("Display_Key");

                entity.Property(e => e.EftposPort)
                    .HasColumnName("Eftpos_Port")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EftposType)
                    .HasColumnName("Eftpos_Type")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EnableCashdraw).HasColumnName("Enable_Cashdraw");

                entity.Property(e => e.EnableEftpos).HasColumnName("Enable_Eftpos");

                entity.Property(e => e.HqMsg)
                    .HasColumnName("HQ_Msg")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.InterfaceId).HasColumnName("Interface_ID");

                entity.Property(e => e.LoginName)
                    .HasColumnName("Login_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MasterIp)
                    .HasColumnName("Master_IP")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PointToVoucher).HasColumnName("Point_toVoucher");

                entity.Property(e => e.PoleBaudRate)
                    .HasColumnName("Pole_BaudRate")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PoleCommand)
                    .HasColumnName("Pole_Command")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PolePort)
                    .HasColumnName("Pole_Port")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PoleText)
                    .HasColumnName("Pole_Text")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrintBarcode).HasColumnName("Print_Barcode");

                entity.Property(e => e.PrintVoucher).HasColumnName("Print_Voucher");

                entity.Property(e => e.PrinterId).HasColumnName("Printer_ID");

                entity.Property(e => e.ScaleManualEntry).HasColumnName("Scale_ManualEntry");

                entity.Property(e => e.ScalePort)
                    .HasColumnName("Scale_Port")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ScaleType)
                    .HasColumnName("Scale_Type")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ScannerPort)
                    .HasColumnName("Scanner_Port")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ShopId).HasColumnName("Shop_ID");

                entity.Property(e => e.ShowDashboard).HasColumnName("Show_Dashboard");

                entity.Property(e => e.ShowMargin).HasColumnName("Show_Margin");

                entity.Property(e => e.ShowPriceKitchen).HasColumnName("Show_Price_Kitchen");

                entity.Property(e => e.TableNo).HasColumnName("Table_No");

                entity.Property(e => e.TerminalIp)
                    .HasColumnName("Terminal_IP")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UseExcel).HasColumnName("Use_Excel");

                entity.Property(e => e.UseIpeftpos).HasColumnName("Use_IPEftpos");

                entity.Property(e => e.VoucherValue)
                    .HasColumnName("Voucher_Value")
                    .HasColumnType("decimal(10, 2)");
            });

            modelBuilder.Entity<TblTranItem>(entity =>
            {
                entity.HasKey(e => e.TranItemId);

                entity.ToTable("tbl_TranItem");

                entity.Property(e => e.TranItemId).HasColumnName("TranItem_ID");

                entity.Property(e => e.Amount).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.ItemId).HasColumnName("Item_ID");

                entity.Property(e => e.ItemName)
                    .HasColumnName("Item_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ItemQty)
                    .HasColumnName("Item_Qty")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OrderId).HasColumnName("Order_ID");

                entity.Property(e => e.PaymentId).HasColumnName("Payment_ID");

                entity.Property(e => e.TerminalId).HasColumnName("Terminal_ID");

                entity.Property(e => e.TranId).HasColumnName("Tran_ID");
            });

            modelBuilder.Entity<TblVoidItem>(entity =>
            {
                entity.HasKey(e => e.VoildId);

                entity.ToTable("tbl_VoidItem");

                entity.Property(e => e.VoildId).HasColumnName("Voild_ID");

                entity.Property(e => e.ItemId).HasColumnName("Item_ID");

                entity.Property(e => e.OrderId).HasColumnName("Order_ID");

                entity.Property(e => e.Qty).HasColumnType("decimal(10, 3)");

                entity.Property(e => e.StaffId).HasColumnName("Staff_ID");

                entity.Property(e => e.VoidAll).HasColumnName("Void_All");

                entity.Property(e => e.VoidDate)
                    .HasColumnName("Void_Date")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VoidTime)
                    .HasColumnName("Void_Time")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblVoucher>(entity =>
            {
                entity.HasKey(e => e.VoucherId);

                entity.ToTable("tbl_Voucher");

                entity.Property(e => e.VoucherId).HasColumnName("Voucher_ID");

                entity.Property(e => e.CustomerId).HasColumnName("Customer_ID");

                entity.Property(e => e.PurchaseDate)
                    .HasColumnName("Purchase_Date")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PurchaseTime)
                    .HasColumnName("Purchase_Time")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Ref)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.UsedDate)
                    .HasColumnName("Used_Date")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UsedTime)
                    .HasColumnName("Used_Time")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VoucherAmount)
                    .HasColumnName("Voucher_Amount")
                    .HasColumnType("decimal(10, 2)");

                entity.Property(e => e.VoucherBarcode)
                    .HasColumnName("Voucher_Barcode")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblWarehouse>(entity =>
            {
                entity.HasKey(e => e.WarehouseId);

                entity.ToTable("tbl_Warehouse");

                entity.Property(e => e.WarehouseId)
                    .HasColumnName("Warehouse_ID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Barcode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CategoryId).HasColumnName("Category_ID");

                entity.Property(e => e.ChineseDesc)
                    .HasColumnName("Chinese_Desc")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Cost).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.ItemCode)
                    .HasColumnName("Item_Code")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ItemName)
                    .HasColumnName("Item_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ShopName)
                    .HasColumnName("Shop_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WarehouseStock)
                    .HasColumnName("Warehouse_Stock")
                    .HasColumnType("decimal(10, 3)");
            });

            modelBuilder.Entity<TblWarehouseLink>(entity =>
            {
                entity.HasKey(e => e.LinkId);

                entity.ToTable("tbl_Warehouse_Link");

                entity.Property(e => e.LinkId).HasColumnName("Link_ID");

                entity.Property(e => e.DatabaseName)
                    .HasColumnName("Database_Name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LinkIp)
                    .HasColumnName("Link_IP")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Login)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblWarehouseQuery>(entity =>
            {
                entity.HasKey(e => e.QueryId);

                entity.ToTable("tbl_WarehouseQuery");

                entity.Property(e => e.QueryId).HasColumnName("Query_ID");

                entity.Property(e => e.ItemId).HasColumnName("Item_ID");

                entity.Property(e => e.Query)
                    .HasMaxLength(1000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblWeekDay>(entity =>
            {
                entity.HasKey(e => e.WeekDayId);

                entity.ToTable("tbl_WeekDay");

                entity.Property(e => e.WeekDayId).HasColumnName("WeekDay_ID");

                entity.Property(e => e.PromotionId).HasColumnName("Promotion_ID");

                entity.Property(e => e.TimeFrom)
                    .HasColumnName("Time_From")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TimeTo)
                    .HasColumnName("Time_To")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WeekDay)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });
        }
    }
}
