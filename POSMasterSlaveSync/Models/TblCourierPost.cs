﻿namespace POSMasterSlaveSync.Models
{
    public partial class TblCourierPost
    {
        public int VanDoorId { get; set; }
        public string UpdateDate { get; set; }
        public string UpdateTime { get; set; }
        public decimal? Total { get; set; }
    }
}
