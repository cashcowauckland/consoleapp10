﻿using System.Collections.Generic;

namespace POSMasterSlaveSync.Models
{
    public partial class TblItem
    {
        public TblItem()
        {
            TblItemSubTab = new HashSet<TblItemSubTab>();
        }

        public int ItemId { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public int? CategoryId { get; set; }
        public decimal? CurrentPrice { get; set; }
        public int? CurrentPoint { get; set; }
        public decimal? TotalStock { get; set; }
        public int? StockLevel { get; set; }
        public string Barcode { get; set; }
        public bool? Indirect { get; set; }
        public bool? Instruction { get; set; }
        public bool? NonAccumlating { get; set; }
        public bool? DisablePoint { get; set; }
        public bool? PriceLevelPoint { get; set; }
        public bool? Status { get; set; }
        public int? RecmdQty { get; set; }
        public bool? IsScaleItem { get; set; }
        public bool? ScaleRange { get; set; }
        public decimal? ScaleRangeMin { get; set; }
        public decimal? ScaleRangeMax { get; set; }
        public decimal? SpecialPrice { get; set; }
        public string ExpiredDate { get; set; }
        public string ChineseDesc { get; set; }
        public decimal? Cost { get; set; }
        public decimal? BoxQty { get; set; }
        public decimal? RemainQty { get; set; }
        public bool? NonDiscount { get; set; }
        public bool? AgeRestricted { get; set; }
        public int? WarehouseId { get; set; }
        public string UpdateDate { get; set; }
        public int? LinkId { get; set; }
        public string ImgPath { get; set; }
        public decimal? CommissionRate { get; set; }
        public bool? UseIndirectPrice { get; set; }
        public bool? HasPriceRange { get; set; }
        public string Comment { get; set; }
        public decimal? LastStockQty { get; set; }
        public bool? Ingredient { get; set; }
        public int? WebItemId { get; set; }
        public bool? Perishable { get; set; }
        public bool? CommentPopup { get; set; }
        public string ScaleUnit { get; set; }
        public bool? AddGst { get; set; }

        public ICollection<TblItemSubTab> TblItemSubTab { get; set; }
    }
}
