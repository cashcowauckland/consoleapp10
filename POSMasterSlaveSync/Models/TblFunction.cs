﻿namespace POSMasterSlaveSync.Models
{
    public partial class TblFunction
    {
        public int FunctionId { get; set; }
        public string FunctionName { get; set; }
        public string FunctionText { get; set; }
        public int? FunctionPosition { get; set; }
        public int? BackColor { get; set; }
        public int? ForeColor { get; set; }
        public string Description { get; set; }
        public int? FontSize { get; set; }
        public bool? Fixed { get; set; }
        public string KeyBoardValue { get; set; }
        public bool? PasswordProtect { get; set; }
    }
}
