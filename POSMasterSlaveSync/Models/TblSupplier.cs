﻿namespace POSMasterSlaveSync.Models
{
    public partial class TblSupplier
    {
        public int SupplierId { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
        public string SupplierPhone { get; set; }
        public string SupplierAddress { get; set; }
        public string SupplierEmail { get; set; }
        public decimal? Discount { get; set; }
        public string SupplierComment { get; set; }
        public string Fax { get; set; }
    }
}
