﻿namespace POSMasterSlaveSync.Models
{
    public partial class TblCheque
    {
        public int ChequeId { get; set; }
        public int? PaymentId { get; set; }
        public string Date { get; set; }
        public string Drawer { get; set; }
        public string Bank { get; set; }
        public string Branch { get; set; }
        public decimal? Total { get; set; }
        public string Description { get; set; }
        public string Identification { get; set; }
        public string ContactDetail { get; set; }
        public bool? Updated { get; set; }
    }
}
