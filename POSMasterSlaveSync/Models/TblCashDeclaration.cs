﻿namespace POSMasterSlaveSync.Models
{
    public partial class TblCashDeclaration
    {
        public int DeclarationId { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public int? PaymentTypeId { get; set; }
        public decimal? Amount { get; set; }
    }
}
