﻿namespace POSMasterSlaveSync.Models
{
    public partial class TblWarehouse
    {
        public int WarehouseId { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public int? CategoryId { get; set; }
        public string ChineseDesc { get; set; }
        public decimal? Cost { get; set; }
        public decimal? WarehouseStock { get; set; }
        public string Barcode { get; set; }
        public string ShopName { get; set; }
    }
}
