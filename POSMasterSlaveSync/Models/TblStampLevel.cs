﻿namespace POSMasterSlaveSync.Models
{
    public partial class TblStampLevel
    {
        public int StampId { get; set; }
        public int? StampLevel { get; set; }
        public int? Stamp { get; set; }
    }
}
