﻿namespace POSMasterSlaveSync.Models
{
    public partial class TblCategory
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public decimal? CommissionRate { get; set; }
        public int? Stamp { get; set; }
        public bool? AllowRedeem { get; set; }
        public int? MainCategoryId { get; set; }
        public decimal? TaxRate { get; set; }
    }
}
