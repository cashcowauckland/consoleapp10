﻿namespace POSMasterSlaveSync.Models
{
    public partial class TblBarcode
    {
        public int BarcodeId { get; set; }
        public int? ItemId { get; set; }
        public string Barcode { get; set; }
    }
}
