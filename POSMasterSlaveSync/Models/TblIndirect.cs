﻿namespace POSMasterSlaveSync.Models
{
    public partial class TblIndirect
    {
        public int IndirectId { get; set; }
        public int? ItemId { get; set; }
        public int? ChildId { get; set; }
        public decimal? Qty { get; set; }
        public decimal? Weight { get; set; }
    }
}
