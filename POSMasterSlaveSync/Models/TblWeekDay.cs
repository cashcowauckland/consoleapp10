﻿namespace POSMasterSlaveSync.Models
{
    public partial class TblWeekDay
    {
        public int WeekDayId { get; set; }
        public int? PromotionId { get; set; }
        public string WeekDay { get; set; }
        public string TimeFrom { get; set; }
        public string TimeTo { get; set; }
    }
}
