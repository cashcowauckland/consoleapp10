﻿using System;

namespace POSMasterSlaveSync.Models
{
    public partial class TblEftposReceipt
    {
        public int EftposReceiptId { get; set; }
        public int? EftposTransId { get; set; }
        public int? MerchantId { get; set; }
        public string ReceiptText { get; set; }
        public string Type { get; set; }
        public DateTime? CreatedOn { get; set; }
    }
}
