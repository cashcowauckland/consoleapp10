﻿namespace POSMasterSlaveSync.Models
{
    public partial class TblPending
    {
        public int PendingId { get; set; }
        public string DateTime { get; set; }
        public string Ref { get; set; }
        public string SaveDate { get; set; }
        public string CustomerId { get; set; }
    }
}
