﻿namespace POSMasterSlaveSync.Models
{
    public partial class TblPromotionItem
    {
        public int ProItemId { get; set; }
        public int? ItemId { get; set; }
        public int? PromotionId { get; set; }
        public int? GroupId { get; set; }
        public int? PromotionGroupId { get; set; }
    }
}
