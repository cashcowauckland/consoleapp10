﻿namespace POSMasterSlaveSync.Models
{
    public partial class TblStaff
    {
        public int StaffId { get; set; }
        public string StaffFirstName { get; set; }
        public string StaffLastName { get; set; }
        public string StaffLogin { get; set; }
        public string StaffPassword { get; set; }
        public int? RoleId { get; set; }
        public int? PriceLevel { get; set; }
        public string StaffBarcode { get; set; }
        public decimal? PayRate { get; set; }
    }
}
