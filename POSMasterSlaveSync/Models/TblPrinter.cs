﻿namespace POSMasterSlaveSync.Models
{
    public partial class TblPrinter
    {
        public int PrinterId { get; set; }
        public int? ShopId { get; set; }
        public string PrinterName { get; set; }
        public string PrinterModel { get; set; }
        public string SelectedPort { get; set; }
        public string Setting { get; set; }
        public string HeaderMessage { get; set; }
        public string FooterMessage { get; set; }
        public string FontType { get; set; }
        public string FontSize { get; set; }
        public string Alignment { get; set; }
        public bool? Emphasize { get; set; }
        public string SubHeaderMessage { get; set; }
        public string Language { get; set; }
    }
}
