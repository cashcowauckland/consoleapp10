﻿namespace POSMasterSlaveSync.Models
{
    public partial class TblShift
    {
        public int ShiftId { get; set; }
        public int? StaffId { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string ShiftDate { get; set; }
        public decimal? PayRate { get; set; }
        public bool? Paid { get; set; }
        public string UploadDate { get; set; }
        public string EndShiftDate { get; set; }
    }
}
