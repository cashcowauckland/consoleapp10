﻿namespace POSMasterSlaveSync.Models
{
    public partial class TblItemPrinter
    {
        public int ItemPrinterId { get; set; }
        public int? ItemId { get; set; }
        public int? PrinterId { get; set; }
        public bool? CheckedStatus { get; set; }
    }
}
