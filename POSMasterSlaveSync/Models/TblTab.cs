﻿namespace POSMasterSlaveSync.Models
{
    public partial class TblTab
    {
        public int TabId { get; set; }
        public string TabName { get; set; }
        public int? PositionNum { get; set; }
    }
}
