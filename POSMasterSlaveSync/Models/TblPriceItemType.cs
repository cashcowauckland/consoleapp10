﻿namespace POSMasterSlaveSync.Models
{
    public partial class TblPriceItemType
    {
        public int PriceId { get; set; }
        public int? ItemPositionId { get; set; }
        public decimal? Amount { get; set; }
        public int? Point { get; set; }
        public int? PriceLevel { get; set; }
    }
}
