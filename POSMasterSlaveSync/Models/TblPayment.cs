﻿namespace POSMasterSlaveSync.Models
{
    public partial class TblPayment
    {
        public int PaymentId { get; set; }
        public int? OrderId { get; set; }
        public int? PaymentTypeId { get; set; }
        public decimal? Amount { get; set; }
        public bool? IsCustomer { get; set; }
        public decimal? AmountOwe { get; set; }
        public string PaymentDate { get; set; }
        public int? TerminalId { get; set; }
    }
}
