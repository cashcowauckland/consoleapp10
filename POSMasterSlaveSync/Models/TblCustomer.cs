﻿namespace POSMasterSlaveSync.Models
{
    public partial class TblCustomer
    {
        public int CustomerId { get; set; }
        public string CustomerFirstName { get; set; }
        public string CustomerLastName { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerMobile { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerDob { get; set; }
        public int? TotalPoint { get; set; }
        public string Barcode { get; set; }
        public int? PriceLevel { get; set; }
        public decimal? CreditLimit { get; set; }
        public decimal? CreditAccount { get; set; }
        public string CustomerWork { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerComment { get; set; }
        public decimal? CreditLeft { get; set; }
        public decimal? DiscountPercentage { get; set; }
        public decimal? LaybyAmount { get; set; }
        public int? Stamp { get; set; }
        public int? FreeItem { get; set; }
        public int? StampLevel { get; set; }
    }
}
