﻿namespace POSMasterSlaveSync.Models
{
    public partial class TblVoucher
    {
        public int VoucherId { get; set; }
        public decimal? VoucherAmount { get; set; }
        public string VoucherBarcode { get; set; }
        public bool? Used { get; set; }
        public string PurchaseDate { get; set; }
        public string PurchaseTime { get; set; }
        public string UsedDate { get; set; }
        public string UsedTime { get; set; }
        public bool? Gift { get; set; }
        public int? CustomerId { get; set; }
        public int? PointToVoucher { get; set; }
        public string Ref { get; set; }
    }
}
