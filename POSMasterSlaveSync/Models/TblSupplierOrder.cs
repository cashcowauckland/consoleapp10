﻿namespace POSMasterSlaveSync.Models
{
    public partial class TblSupplierOrder
    {
        public int SupplierOrderId { get; set; }
        public string Date { get; set; }
        public int? SupplierId { get; set; }
        public bool? Acked { get; set; }
        public string InvoiceNo { get; set; }
        public string Comment { get; set; }
    }
}
