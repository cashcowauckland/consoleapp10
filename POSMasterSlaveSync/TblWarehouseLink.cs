﻿namespace Temp.Models
{
    public partial class TblWarehouseLink
    {
        public int LinkId { get; set; }
        public string LinkIp { get; set; }
        public string DatabaseName { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
