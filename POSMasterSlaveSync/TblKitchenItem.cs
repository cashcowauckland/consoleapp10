﻿namespace Temp.Models
{
    public partial class TblKitchenItem
    {
        public int KitchenItemId { get; set; }
        public int OrderId { get; set; }
        public decimal Qty { get; set; }
        public string ItemName { get; set; }
        public string Desc2 { get; set; }
        public int CategoryId { get; set; }
        public bool Instruction { get; set; }
        public byte ItemStatus { get; set; }
        public byte OrderStatus { get; set; }
        public byte SequenceId { get; set; }
        public bool ServiceSameTime { get; set; }
        public int TerminalId { get; set; }
        public int StaffId { get; set; }
        public string OrderDate { get; set; }
        public string OrderTime { get; set; }
    }
}
