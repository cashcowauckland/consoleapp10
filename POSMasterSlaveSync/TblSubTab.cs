﻿namespace Temp.Models
{
    public partial class TblSubTab
    {
        public int SubTabId { get; set; }
        public string SubTabName { get; set; }
        public int? TabId { get; set; }
        public int? PositionNum { get; set; }
    }
}
