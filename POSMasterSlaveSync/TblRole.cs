﻿namespace Temp.Models
{
    public partial class TblRole
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
    }
}
