﻿namespace Temp.Models
{
    public partial class TblBlockPosition
    {
        public string BlockNum { get; set; }
        public int? BlockId { get; set; }
    }
}
