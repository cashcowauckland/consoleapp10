﻿namespace Temp.Models
{
    public partial class TblKitchenMonitor
    {
        public byte SetupId { get; set; }
        public int KitchenItemId { get; set; }
        public int OrderId { get; set; }
        public byte OrderStatus { get; set; }
    }
}
