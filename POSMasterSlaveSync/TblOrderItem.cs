﻿namespace Temp.Models
{
    public partial class TblOrderItem
    {
        public int OrderItemId { get; set; }
        public int? ItemId { get; set; }
        public int? OrderId { get; set; }
        public decimal? DiscountP { get; set; }
        public decimal? DiscountD { get; set; }
        public decimal? ItemPrice { get; set; }
        public decimal? ItemQty { get; set; }
        public int? ItemTypeId { get; set; }
        public decimal? Cost { get; set; }
        public string TrackBarcode { get; set; }
        public bool? NonDiscountable { get; set; }
        public int? GroupDiscountId { get; set; }
        public int? TerminalId { get; set; }
        public decimal? CommissionRate { get; set; }
    }
}
