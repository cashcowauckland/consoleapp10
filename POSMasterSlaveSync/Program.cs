﻿using System;
using POSMasterSlaveSync.Models;

namespace POSMasterSlaveSync
{
    class Program
    {
        static void Main(string[] args)
        {
            CallSync();
        }

        private static void CallSync()
        {
            Console.WriteLine("POSMasterSlaveSync in Progress DB..POSSystemTest");

            SyncItemType();

            Console.WriteLine("POSMasterSlaveSync 1 record added and others in progress..");
            while (true) { }
        }

        private static void SyncItemType()
        { 
            POSMasterSlaveSync.Models.PosSystemSlave1Context context = new PosSystemSlave1Context();

            
            TblItemType itmType = new TblItemType();
            //itmType.ItemTypeId = 10;
            itmType.GroupName = "FoodGroup12345678";
            itmType.TypeName  = "Gravy12345678";
            context.Add(itmType);
            context.SaveChanges();

            TblMainCategory itmTypecate = new TblMainCategory();
            //itmType.ItemTypeId = 10;
            itmTypecate.MainCategoryName = "KIWIsFOOD";

            context.Add(itmTypecate);
            context.SaveChanges();

        }

    }
}
